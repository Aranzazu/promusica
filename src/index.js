import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './normalize.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import Sala from './components/Sala';
import { Test1 } from  './components/Test1';
import { Espectaculo } from './components/Espectaculo';
import ListadoEspectaculos from './components/ListadoEspectaculos';
import { Tarifas } from './components/Tarifas';
import {ListadoSala} from './components/ListadoSala';
import {VerSala} from './components/VerSala';
import {Riders} from './components/Riders';
import {Login} from './components/Login';
import { Stitch, UserPasswordCredential, RemoteMongoClient } from 'mongodb-stitch-browser-sdk';
import Bolo from './components/Bolo';
import { ListadoBolos } from './components/ListadoBolos';
import Grupo from './components/Grupo';
import ListadoGrupo from './components/ListadoGrupo';
import Artista from './components/Artista';
import ListadoArtistas from './components/ListadoArtistas';
import Admin from './components/Admin';










export const stitch = Stitch.initializeDefaultAppClient('promusic-ohiww')
export const mongo = stitch.getServiceClient(RemoteMongoClient.factory,'mongodb-atlas')
// ponemos el if para que no cree un credencial anonimo si ya conoce el navegador (dominio) y por direccion IP

// stitch.callFunction('getValue', ['Aran']).then()

export function jsonToFile(obj = {},  filename = 'archivoSinNombre' ) {
  let e = document.createEvent('MouseEvents'),
      a = document.createElement('a'),        
      blob = null;
  blob = new Blob([JSON.stringify(obj, undefined, 2)], {type: 'text/json'});                 
  a.download = filename;
  a.href = window.URL.createObjectURL(blob);     
  a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');    
  e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  a.dispatchEvent(e);   
}


export function nbsp(n) {
  let ret = []
  for (let index = 1; index <= n; index++) {
    ret.push(<span key={index}>&nbsp;</span>);    
  }
  return ret
}


export function br(n) {
  let ret = []
  for (let index = 1; index <= n; index++) {
    ret.push(<br key={index} />);    
  }
  return ret
}


const rutas = (
<BrowserRouter>

<div className="barra">
{nbsp(5)}
  <Link to="/listaS" style={{textDecoration: 'none'}}> <span className="sombreado" style={{color:"#572364", fontWeight: "bold"}}>SALAS</span> </Link>{nbsp(5)}
  <Link to="/listaE" style={{textDecoration: 'none'}}> <span className="sombreado" style={{color:"#572364", fontWeight: "bold" }}>ROSTER</span> </Link>{nbsp(5)}
  <Link to="/listaG" style={{textDecoration: 'none'}}> <span className="sombreado" style={{color:"#572364", fontWeight: "bold" }}>GRUPOS</span> </Link>{nbsp(5)}
  <Link to="/listaA" style={{textDecoration: 'none'}}> <span className="sombreado" style={{color:"#572364", fontWeight: "bold" }}>ARTISTAS</span> </Link>{nbsp(5)}
  <Link to="/listaB" style={{textDecoration: 'none'}}> <span className="sombreado" style={{color:"#572364", fontWeight: "bold" }}>BOLOS</span> </Link>{nbsp(5)}
  <Link to="/admin" style={{textDecoration: 'none'}}> <span className="sombreado" style={{color:"#572364", fontWeight: "bold" }}>ADMIN</span> </Link>{nbsp(5)}
  <Link to="/login">
    <i className="material-icons" title="login" style={{color:"#572364", fontWeight: "bold", float: "right" }}>
    music_note
    </i></Link>
    {/* info sobre si estas o no log */}
  <span style={{color:"#0d6e79", fontWeight: "bold", float: "right" }}>
    { stitch.auth.isLoggedIn ? stitch.auth.user.profile.email : ''}
  </span> 
  
  

  {/* <Link to="/fichaSala/:id"> Ficha Sala </Link> {nbsp(5)} */}
</div>
<Route exact path="/" component={ ListadoBolos } />
<Route  path="/login" component={ Login } />
<Route  path="/admin" component={ Admin } />
<Route  path="/test" component={ Test1 } />
<Route  path="/listaS" component={ ListadoSala } />
<Route  path="/listaE" component={ ListadoEspectaculos } />
<Route  path="/listaB" component={ ListadoBolos } />
<Route  path="/listaG" component={ ListadoGrupo } />
<Route  path="/listaA" component={ ListadoArtistas } />
<Route  path="/bolo/:id" component={ Bolo } />
<Route  path="/fichaSala/:id" component={ VerSala } />
<Route  path="/riders/:id" component={ Riders } />
<Route  path="/sala/:id" component={ Sala } />
<Route  path="/tarifas/:id" component={ Tarifas }/> 
<Route  path="/espectaculo/:id" component={ Espectaculo } />
<Route  path="/grupo/:id" component={ Grupo } />
<Route  path="/artista/:id" component={ Artista } />
</BrowserRouter>
)

ReactDOM.render(rutas, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// Firebase functions


/* firebase.initializeApp(config.firebaseConfig);



export const uploadFile = file => {
  // de momento este Storage no tiene reglas así que funciona sin autenticación;
  // en otro caso habrá que habilitar esto:
  // var provider = new firebase.auth.GoogleAuthProvider();
  // firebase.auth().signInWithPopup(provider);
  let filePath = `/pruebasReact/${file.name}`
  return firebase.storage().ref(filePath).put(file)
  .then( fileSnapshot => {    
    return fileSnapshot.ref.getDownloadURL().then( url => { return url } )   
  })
} */

/* db.artistas.updateMany({},{$set: {autor:"5e175bc8c90b4af0eb978734"}}) esto es para reemplazar es importante el operador atomico $set un campo por otro en la bbdd $unset para eliminar un campo*/