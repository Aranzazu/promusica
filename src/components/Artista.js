import React, { Component } from 'react'
import { mongo, stitch, nbsp, br } from '../index'
import {ObjectID} from 'bson';
import { EsquemaGrupo } from './Grupo';

// como hacer cruces de dos tablas
export class EsquemaArtistas {
  constructor(){
    this.nombre = ''; //ok
    this.apellido = ''; // ok
    this.competencias = []; //ok
    this.telefono = '';//ok
    this.comentario = '';//ok
    this.autor = "5e175bc8c90b4af0eb978734";
    this.jefes = []
  }
  

  insertOneToDb() {
    return stitch.callFunction('getValue', ['conf']).then(conf =>{
      this.autor = conf.autor
      this.jefes = conf.jefes
    return mongo.db('proMusic').collection('artistas').insertOne(this)
    })
   }
   findOneAndReplaceToDb(id){
    return stitch.callFunction('getValue', ['conf']).then(conf =>{
      this.autor = conf.autor
      this.jefes = conf.jefes
    return mongo.db('proMusic').collection('artistas').findOneAndReplace({_id: new ObjectID( id )}, this)
    }) 
    }
   deleteOneToDb(id){
    return mongo.db('proMusic').collection('artistas').deleteOne({_id: new ObjectID(id)})
   }

   static getOne(id){
     return mongo.db('proMusic').collection('artistas').findOne({_id: new ObjectID(id)})
   }

   static listadoArtistas(){
    return mongo.db('proMusic').collection('artistas').aggregate([
      {$sort: {nombre: 1}}
    ]).toArray()
  }

  static buscarArtistaPorNombre(nombre) {
    return mongo.db('proMusic').collection('artistas').aggregate([
      {$match:{ nombre: {$regex: nombre, $options:'i'}}},
      {$sort:{ nombre: 1}} // no estoy segura de que esto funcione.
    ]).toArray()
  }
 
  static listadoCompetenciasArtistas(nombre){
  
    return mongo.db('proMusic').collection('artistas').aggregate([
      { $match: { "competencias.competencia": { $regex: nombre ? nombre: '', $options:'i'}}},
     
    ])
    .toArray()
    }
  static listadoCompetencias(){
    return mongo.db('proMusic').collection('artistas').aggregate([
      { $unwind: "$competencias"},
      { $sortByCount: "$competencias.competencia"},
    ]).toArray()
  }
  
    
}


export class CompetenciasCache {
  constructor(){
    this.competencia = '';
    this.cache = 0;
  }
}

export default class Artista extends Component {

  componentDidMount(){
    EsquemaArtistas.listadoArtistas().then( l => console.log(l))

    if (this.props.match.params.id === 'nuevo') {      
      this.setState({
       artista: new EsquemaArtistas()
      })
    }
    else { this.getOne() }
  }
  
  getOne = n => {
    EsquemaArtistas.getOne(this.props.match.params.id).then( a => {console.log(a); this.setState({artista: Object.assign(new EsquemaArtistas(), a)})})
    .catch(e => {console.log(e)})
  }

  // si lo hacemos aso hacemos los onchanege variables llamando a esta funcion, poniendo un id o name a cada input
  changeInput = e => {
    this.setState({
      artista:Object.assign(new EsquemaArtistas(), {...this.state.artista, [e.target.id]: e.target.value}) 

    })
  }

  renderInputArtista = () => {
    return(
      <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
  
         {/* <label className="h4">Artista</label> */}
         <input className="form-control"
         id="nombre"
         placeholder="Nombre"
         size="20"
         value={this.state.artista.nombre}
         onChange={ this.changeInput} /> {nbsp(1)}
  
         {/* <label className="h2">Apellido</label> */}
         <input className="form-control"
         id="apellido"
         placeholder="Apellido"
         size="20"
         value={this.state.artista.apellido}
         onChange={ this.changeInput} />{nbsp(1)}
  
        {/* <label className="h3">Teléfono</label> */}
         <input className="form-control"
         id="telefono"
         placeholder="Teléfono"
         value={this.state.artista.telefeno}
         onChange={this.changeInput}/>
     
      </div>
    )
  
  }
  renderInputCompetenciasCache = () => {
    return(
      <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
  
        {/* <label className="h3">competencias</label> */}
         <input className="form-control"
         id="competencia"
         placeholder="competencia"
         size="20"
         value={this.state.nuevaCompetencia.competencia}
         onChange={e => this.setState({ nuevaCompetencia:Object.assign(new CompetenciasCache (),{...this.state.nuevaCompetencia, competencia: e.target.value})  })} />{nbsp(1)}
  
        {/* <label className="h3">Cache</label> */}
          <input className="form-control" 
         id="cahe"
         placeholder="Cache"
         size="5"
         type = "number"
         value={this.state.nuevaCompetencia.cache}
         onChange={e => this.setState({ nuevaCompetencia:Object.assign(new CompetenciasCache (),{...this.state.nuevaCompetencia, cache: e.target.value})  })}/>{nbsp(1)}
  
         {/* añade al array de competencias y cache */} 
        <i className="material-icons" title="añade"
       style={{color:"#753086"}}
       onClick={ e => {
        let copia = this.state.artista.competencias
        copia.push(this.state.nuevaCompetencia)
        this.setState({ artista: Object.assign(new EsquemaArtistas(), {...this.state.artista, competencias: copia })})
        console.log(this.state.artista)
     }}> check_circle </i> 
  
      </div>
    )
  } 
  renderInputComentarioArtista = () => {
    return(
      <div style={{ margin: "10px 15px 10px 15px"}}>
        <label className="h3">Comentario</label>
        <textarea className="form-control"
          id="comentario"
          placeholder="comentarios"
          rows="5"
          value = {this.state.artista.comentario}
          onChange = {this.changeInput}
          />
        
  
      </div>
    )
  }
  renderIconoGrabar = () => {
    return(
      <i className="material-icons"
           style={{color: "#753086"}}  
           title="grabar"
           hidden={!this.props.match.params.id}
             onClick={() => {
              this.state.artista.insertOneToDb()
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }}>save</i>
    )
  }  
  renderIconoEditar   = () => {
    return(
      <i className="material-icons"
             style={{color:"#753086"}} 
            title="editar"
            hidden={!this.props.match.params.id}
            onClick={ () => { this.state.artista.findOneAndReplaceToDb(this.props.match.params.id )
              .then( d => {console.log( d )})
              .catch( e => {console.log( e )})} 
            } >create</i>
    )
  }
  renderIconoBorrar = () => {
    return(
      <i className="material-icons" 
            title="Borar"
            style={{color:"#8B0000"}}
            hidden={!this.props.match.params.id} 
            onClick={ e =>{this.state.artista.deleteOneToDb(this.props.match.params.id)
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }} >
          delete_forever
        </i>
    )
  }

  state = {artista: new EsquemaArtistas(), nuevaCompetencia: new CompetenciasCache()}

  render() {
    return (
      <div>
         <div className = "text-center">
            <span className="h2"> {this.state.artista.nombre} </span>{br(1)}
            {/*grabar la espectaculo  */}
            {this.renderIconoGrabar()}{nbsp(2)}
            {/*edita la espectaculo  */}
            {this.renderIconoEditar()} {nbsp(2)}
            {/*Borra la espectaculo  */}
            {this.renderIconoBorrar()}
          </div><hr/>

          <div className = "text-center">
            <span className="h4"> {this.state.artista.nombre} </span> 
            <div className = "form-inline">
            {this.renderInputArtista()}
            </div>
            {this.renderInputComentarioArtista()}
            {this.renderInputCompetenciasCache()}
              
          </div><hr/>


       <pre> { JSON.stringify(this.state, undefined,2)}</pre> 
      </div>
    )
  }
}
