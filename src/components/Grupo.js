import React, { Component } from 'react';
import { mongo, stitch, nbsp, br } from '../index'
import {ObjectID} from 'bson';
import {EsquemaArtistas} from './Artista';

export class EsquemaGrupo {
  constructor() {
    this.nombre = ''; // ok
    this.componentes = [];
    this.comentario = '';
    this.autor = "5e175bc8c90b4af0eb978734";
    this.jefes = []
  }

  
  

  insertOneToDb() {
    return stitch.callFunction('getValue', ['conf']).then(conf =>{
      this.autor = conf.autor
      this.jefes = conf.jefes
   return mongo.db('proMusic').collection('grupos').insertOne(this)
    })
  }

  findOneAndReplaceToDb(id){
    return stitch.callFunction('getValue', ['conf']).then(conf =>{
      this.autor = conf.autor
      this.jefes = conf.jefes
    return mongo.db('proMusic').collection('grupos').findOneAndReplace({_id: new ObjectID( id )}, this)
    })   
  }
  deleteOneToDb(id){
   return mongo.db('proMusic').collection('grupos').deleteOne({_id: new ObjectID(id)})
  }

  static listadoGrupo(){
    return mongo.db('proMusic').collection('grupos').aggregate([
      {$sort: {nombre: 1}}
    ]).toArray()
  }
  static listadoArtistaGrupo(){
    return mongo.db('proMusic').collection('grupos').aggregate([
      {$lookup:{
        from:'artistas',
        localField:'componentes',
        foreignField:'_id',
        as: 'artistas'
      }}
    ]).toArray()
  }

  static listadoArtista(nombre){
  
    return mongo.db('proMusic').collection('artistas').aggregate([
      { $sort: "$componentes"},
      { $sortByCount: "$componentes.nombre"},// mongo nos pone el nombre como _id
      { $match: {_id: { $regex: nombre ? nombre: '', $options:'i'}}}
    ])
    .toArray()
    }

  static mediaCacheArtista(palabra, sort){
    return mongo.db('proMusic').collection('grupos').aggregate([
      { $unwind: "$componentes"},
      { $unwind:"$componentes.competencias"},
      { $group: {
        _id: { nombre: "$componentes.nombre", apellido:"$componentes.apellido", competemcias:"$componentes.competencias.competencia"},
        competencias:{ $sum: 1},
        cache: { $avg: "$componentes.competencias.cache"},
      }},
      {$match: {$or:[
        {"_id.nombre":palabra},
        {"_id.apellido":palabra},
        {"_id.competencia":palabra},
      ]}}, 
      {$sort: sort ? sort:{cache: -1}}
    ]).toArray() 
  }  
  static buscarGrupoPorNombre(nombre) {
      return mongo.db('proMusic').collection('grupos').aggregate([
        {$match:{ nombre:{$regex: new RegExp(nombre), $options:'i'}}},
        {$sort:{nombre: nombre ? nombre: ''}} // no estoy segura de que esto funcione.
      ]).toArray()
    }

  static listadoCompetencias(competencia){
    return mongo.db('proMusic').collection('grupos').aggregate([
      {$unwind:"$componentes"},
      {$unwind:"$componentes.competencias"},
      {$sortByCount:"$componentes.competencias.competencia"},
      {$match:{_id:{ $regex: competencia ? competencia: '', $options:'i'}}}
    ])
  }  
}


export default class Grupo extends Component {

  componentDidMount(){
    EsquemaGrupo.listadoGrupo().then( l => console.log(l))
    EsquemaArtistas.listadoArtistas().then( a => this.setState({listArtistas: a}))

    if (this.props.match.params.id === 'nuevo') {      
      this.setState({
       grupo: new EsquemaGrupo()
      })
    }
    else { this.getOne() }
  }

  getOne = n =>{

    let idBueno = new ObjectID(this.props.match.params.id)
    this.mongoGrupo.findOne({_id: idBueno})
    .then( g => {console.log(g); this.setState({grupo: Object.assign(new EsquemaGrupo(), g)})})
    .catch(e => {console.log(e)})
  }

  anadirArtistaGrupo = (a) => {
    let copia = this.state.grupo.componentes
    copia.push(Object.assign(new EsquemaArtistas(), a))
    this.setState( {
      grupo: Object.assign(new EsquemaGrupo(), {...this.state.grupo, componentes: copia })
    }) 
    
    let indexArtista = this.state.listArtistas.findIndex( artista =>  artista._id === a._id)
    let copiaArtistas =  this.state.listArtistas
    copiaArtistas.splice(indexArtista,1) 
     console.log()
  }
  borrarCompetencia = (i) =>{
    let copia = this.state.grupo.componentes.competencias
    copia.splice(i,1)
    this.setState({
      grupo: {...this.state.grupo, competencias: copia}
    })
  }
  
  renderInputNombreGrupo = () => {
    return(
      <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
        <input className="form-control"
          placeholder="Nombre Grupo" 
          size="35"
          value={this.state.grupo.nombre}
          onChange={ e => {this.setState({
            grupo: Object.assign( new EsquemaGrupo(),{...this.state.grupo, nombre: e.target.value})
          })}
        }/>
      </div>
    )
  
  }
 
  renderInputComentario = () => {
    return(
      <div style={{ margin: "10px 15px 10px 15px"}}>
        <label className="h3">Comentario</label>
        <textarea className="form-control"
          placeholder="comentarios"
          rows="5"
          value = {this.state.grupo.comentario}
          onChange = { e => {this.setState({
            grupo: Object.assign( new EsquemaGrupo(), {...this.state.grupo, comentario: e.target.value})
          })}}
          />
        
  
      </div>
    )
  }

  renderIconoGrabar = () => {
    return(
      <i className="material-icons"
           style={{color: "#753086"}}  
           title="grabar"
           hidden={!this.props.match.params.id}
             onClick={() => {
              this.state.grupo.insertOneToDb()
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }}>save</i>
    )
  }  
  renderIconoEditar   = () => {
    return(
      <i className="material-icons"
             style={{color:"#753086"}} 
            title="editar"
            hidden={!this.props.match.params.id}
            onClick={ () => { this.state.grupo.findOneAndReplaceToDb(this.props.match.params.id )
              .then( d => {console.log( d )})
              .catch( e => {console.log( e )})} 
            } >create</i>
    )
  }
  renderIconoBorrar = () => {
    return(
      <i className="material-icons" 
            title="Borar"
            style={{color:"#8B0000"}}
            hidden={!this.props.match.params.id} 
            onClick={ e =>{this.state.grupo.deleteOneToDb(this.props.match.params.id)
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }} >
          delete_forever
        </i>
    )
  }
  renderButtonAnadiABolo = () =>{
    return(
      <i className="material-icons"
        style={{color:"#0d6e79"}} 
        title="añadir a bolo"
        onClick={ e => { 
          // console.log(g)
          this.props.location.enviarABolo('grupos', this.state.grupo) 
          this.props.history.goBack()
        }}
          >check_circle</i>
    )   
  }

  mongoGrupo = mongo.db('proMusic').collection('grupos')

  state = { 
    grupo: new EsquemaGrupo(), 
    artista: new EsquemaArtistas(),
    listArtistas: []
  }

  render() {
    return (
      <div>
        <div className = "text-center">
          {/* esto: {this.props.location.hola} <hr /> */}
            <span className="h2"> {this.state.grupo.nombre} </span>{br(1)}
            <div hidden={this.props.location.desdeBolos}>
              {/*grabar la espectaculo  */}
              {this.renderIconoGrabar()}{nbsp(2)}
              {/*edita la espectaculo  */}
              {this.renderIconoEditar()} {nbsp(2)}
              {/*Borra la espectaculo  */}
              {this.renderIconoBorrar()}
              {this.renderInputNombreGrupo()}{nbsp(2)}
              
            </div>
            <div hidden={!this.props.location.desdeBolos}>
               {/*añadir a bolo */}
               {this.renderButtonAnadiABolo()}
            </div>
            
            {this.state.grupo.componentes.map(
              (a,i) => { return(
                <div key={i}>
                  {a.nombre}:{nbsp(2)}{a.competencias.map( 
                    (c,i) => { return ( 
                    <div key={i}>
                      {c.competencia},{nbsp(2)}{c.cache}€
                    <i className="material-icons" 
                        title="Borar"
                        style={{color:"#8B0000", float:"right"}}
                        onClick={ e => { 
                          let copia = a.competencias
                          copia.splice(i,1)
                          this.setState({ 
                            artista: Object.assign( new EsquemaArtistas(), {...a, competencias: copia})  
                        })
                      }     
                      }>delete_forever
                    </i>
                      
                    
                    </div> )
                  })}
                </div>
              )}
            )}
          
            {this.renderInputComentario()}
          </div><hr/>

          {this.state.listArtistas.map( (a, i) => <VerArtista key = {i}  artista={a} anadirArtistaGrupo ={this.anadirArtistaGrupo}  /> )}


       <pre> { JSON.stringify(this.state, undefined,2)}</pre> 
      </div>
    )
  }
}

export class VerArtista extends Component {

  renderButtonAnadiAGrupo = () =>{

    return(
      <i className="material-icons"
        style={{color:"#0d6e79"}} 
        title="añadir a grupo"
        onClick={ e => { this.props.anadirArtistaGrupo(this.props.artista ) }}
         >check_circle</i>
    )
  }
  
  render(){
    return(
      <div className="fichaArtGrup text-center">

       <span className="h3"> {this.props.artista.nombre} </span> 

        {this.props.artista.competencias.map( (c,i) => {return(
          <div key={i} className = "form-inline" >
            {c.competencia}{nbsp(2)}  
             Cache: {c.cache}{nbsp(2)} 
            
          </div>
        )}
        )}
        {br(1)}
        {this.renderButtonAnadiAGrupo()}
        {JSON.stringify(this.state, null, 2)}
      </div>
    )
  }


}



