import React, { Component } from 'react'
import { mongo,nbsp, br, } from '../index'
import { Link } from 'react-router-dom';
import { EsquemaSala } from './Sala';
import {ObjectID} from 'bson';


export class VerSala extends Component {
  state={ sala: new EsquemaSala()}
  mongo = mongo.db('proMusic').collection('salas')

  componentDidMount(){
    this.getOne()
  
  }

  getOne = s =>{  
    let idSala = new ObjectID(this.props.match.params.id)
    this.mongo.findOne({_id: idSala})
    .then(s => {
      this.setState({sala: s})
    } )}

    render() {

      return(
        <div>
          <img src={this.state.sala.imagenes.exterior} alt="ext" className="izquierda fotos" />
          <img src={this.state.sala.imagenes.interior} alt="int" className="derecha fotos" />
          <div className="text-center">
            <div className="h1 text-center">{this.state.sala.nombre} </div>
            <Link to={`/sala/${this.state.sala._id}`}><span  style={{color:"#0d6e79", fontWeight: "bold", float: "right"}}>  <i className="material-icons" title="editar sala"> create</i></span></Link>
            <div className="h2 text-center">{this.state.sala.ciudad} </div><hr/>
            <span className="titulo">
            Dirección:</span>{nbsp(2)}
            <span className="contenido">{this.state.sala.direccion} 
            </span>{nbsp(5)}
            <span className="titulo">
            Contacto:</span>{nbsp(2)}
            <span className="contenido">{this.state.sala.contacto}
            </span>{nbsp(5)}
            <span className="titulo">  
            Teléfono:</span>{nbsp(2)}
            <span className="contenido">{this.state.sala.telefono}
            </span>{br(2)}
          <div className="text-center">
            <span className="titulo">
            email:</span>{nbsp(2)}
            <span className="contenido">{this.state.sala.mail}
            </span>{nbsp(5)}
          </div>      
          <hr/>
       
          <div className="text-center">
            <span className="titulo"> Aforo:</span>{nbsp(2)}
            <span className="contenido">{this.state.sala.aforo}
            </span>{nbsp(5)}
            <span className="titulo" hidden={!this.state.sala.tarifaCancelacion }>  Cancelación:</span>{nbsp(2)}
            <span className="contenido" hidden={!this.state.sala.tarifaCancelacion }>{this.state.sala.tarifaCancelacion}</span>
            {nbsp(5)}
            <span className="titulo" hidden={!this.state.sala.parking.tiene }> Parking:</span>{nbsp(2)}
            <span className="contenido"  hidden={!this.state.sala.parking.tiene } >{this.state.sala.parking.concepto}
            </span>{nbsp(5)}
            <span hidden={!this.state.sala.publicidad} className="titulo"> Publicidad:</span>{nbsp(2)}
            <span hidden={!this.state.sala.publicidad} className="contenido">{this.state.sala.publicidad} 
            </span> 
            
            {nbsp(5)}
              <a href={this.state.sala.contrato} target="_blank" className="titulo" rel="noopener noreferrer"  > Contrato PDF</a>
                {nbsp(5)}
              <a href={this.state.sala.riderPdf} target="_blank" className="titulo" rel="noopener noreferrer">Rider PDF</a>
              <hr />
          </div> 
          </div>

          <div>
          <div style={{padding: "20px"}}>
            <span className="titulo ">Caracteristicas:</span>{br(1)}
            <div dangerouslySetInnerHTML={{ __html: this.state.sala.caracteristicas }} />
          
          </div>
        
          </div>
          {br(1)} 
          <hr/>
            <div style={{padding: "20px"}} >
            <div className="listaTarifas" > 
             <span className="h2">Tarifas:</span> 
            <span className="contenido">{this.state.sala.tarifas.map(
              (t,i) => {
                return(
                  <div key={i} className="form-inline" >
                    <div style={{color: "#753086", fontWeight: "bold", width:"45%"}} >{t.concepto}:</div>{nbsp(2)}
                    <div style={{color:"#8B0000", width:"17%"}}>{t.coste}€</div>{nbsp(2)}
                    <div style={{color:"green",  width:"17%"}} hidden={!t.beneficio}>{t.beneficio}</div>
                  </div>
                )
              }
              
            )}
            </span>
          </div>  
          <div className="listaRider">
          <span className="h2 ">Rider:</span>{br(1)}
          {this.state.sala.rider.map( (r,i) => {return (<ShowRiders key={i} {...r}/>)})}
          </div>
          </div>
      </div>
      )}
}                      

class ShowRiders extends Component {
  render(){
    return(
      <div className=" form-inline">
          <div className="contenido"style={{color: "#753086",  fontWeight: "bold", width:"15%"}} >{this.props.concepto}:</div>{nbsp(2)}
          <div style={{fontWeight: "bold", width:"50%"}}>{this.props.comentarios}</div>{nbsp(2)}
      </div>
    )
  }
}

