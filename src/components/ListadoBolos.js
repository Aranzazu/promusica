import React, { Component } from 'react'
import { mongo, nbsp, br } from '../index'
import { Link } from 'react-router-dom';
import { EsquemaBolo } from './Bolo';


export class ListadoBolos extends Component {

  state = { bolos:[], fechaDesde:'', fechaHasta:''}

  mongoBolos = mongo.db('proMusic').collection('bolos')

  getBolosPdt = () => {
    this.mongoBolos.aggregate([
      {$match: {fecha: {$gte:new Date().toISOString().split('T')[0] } }},
      {$sort: {fecha: 1, nombre: 1}}
    ]).toArray().then( b => {this.setState({bolos:b}); console.log(b)})
  }

  getTodosLosBolos = () => {
    this.mongoBolos.aggregate([
      {$sort: {fecha: 1}}
    ]).toArray().then( b => {this.setState({bolos:b}); console.log(b)})
  }

  componentDidMount(){

    this.getBolosPdt()
  }

  InputBusquedaXFecha(){
    return(
      <div className="form-inline">
        Buscar desde{nbsp(2)}
        <input type="date" name="desde" className="form-control"
        value = {this.state.fechaDesde}
        onChange = {e =>{
          this.setState({ fechaDesde: e.target.value})

        }}
        />{nbsp(2)}
         Buscar hasta{nbsp(2)}
         <input type="date" name="hasta"  className="form-control"
        value = {this.state.fechaHasta}
        onChange = {e =>{
          this.setState({ fechaHasta: e.target.value}, () => {
            EsquemaBolo.busquedaPorFecha(this.state.fechaDesde, this.state.fechaHasta).then(res => {this.setState({bolos:res})})
          })
          
        }}
        />
      </div>
    )
  }

  render() {
    return (
      <div  style={{ margin: "10px 15px 10px 15px"}}>
        <div className="form-inline">

        {this.InputBusquedaXFecha()}{nbsp(2)}
        <span className="decoracion" onClick={e=>{ this.getTodosLosBolos()}} >Todos</span>{nbsp(2)}
        <Link to = {'/bolo/nuevo'}  style={{ textDecoration: 'none'}}> <span className="decoracion" >Nuevo</span></Link>
        </div>
      {this.state.bolos.map(
        (b,i) => {
          let bolos = Object.assign(new EsquemaBolo(), b)
          return(
            <div key={i}  style={{ margin: "10px 15px 10px 15px"}} >
             <div  className="ficha">
             <img src={bolos.imagen} style={{float: "left"}} alt="img" className="fotolistaEspectaculo"/>
             <div className="text-center">
             
              <Link to={`/bolo/${bolos._id}`} className="h2">{bolos.nombre}</Link>{br(1)}
              <span style={ {color:'red' ,fontWeight:'bold' }}><u>{bolos.fechaOrden()}</u></span>{nbsp(1)}-{nbsp(1)}
              <span style={ {color:'#753086' ,fontWeight:'bold' }}>{bolos.sala.ciudad}:{nbsp(1)} {bolos.sala.aforo}(Aforo)</span>
              <hr/>  
              <span className="h3"> {bolos.sala.nombre}</span>{nbsp(2)}
              
              </div>
              {bolos.espectaculos.map((e,i)=>{
                return (
                <div key={i}>
                {e.nombre}{nbsp(2)} 
{/*                   <video width="100" height="100" controls style={{float: "right"}} className=" fotolistaEspectaculo">
                      <source src={e.video} type="video/mp4"/>
                    </video>
                    
 */}                
 
              </div>  
                  )
              })}
              {bolos.grupos.map( (g,i)=>{
                return(
                  <div key={i} className="form-inline">
                    <u>{g.nombre}</u>:{nbsp(1)} {g.componentes.map( (c,i) =>{
                      return(
                        <div key={i} className="form-inline">
                          {c.nombre}:{nbsp(1)}{c.competencias.map((x,i) =>{
                            return(
                              <div key={i}>
                                {x.competencia};{nbsp(2)}
                              </div>
                            )
                          })}
                        </div>
                      )
                    })}

                  </div>
                )
              })}
             </div>
            </div>
          )})}
      </div>    
    )
  }
}


