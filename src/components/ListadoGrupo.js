import React, { Component } from 'react'
import { mongo, nbsp, br, jsonToFile} from '../index'
import { Link } from 'react-router-dom';
import { EsquemaGrupo } from './Grupo';




export default class ListadoGrupo extends Component {

  mongoGrupo = mongo.db('proMusic').collection('grupos')

  state = {grupos:[], busquedaNombreGrupo:''}

  componentDidMount(){
    EsquemaGrupo.listadoGrupo().then( g =>{
      this.setState({grupos: g})
    })

    }
  renderButtonAnadiABolo = (grupo) =>{
    return(
      <i className="material-icons"
        style={{color:"#0d6e79"}} 
        title="añadir a bolo"
        onClick={ e => { 
          console.log(grupo)
          this.props.enviarABolo('grupos', grupo) 
          let index = this.state.grupos.findIndex( e => grupo._id === e._id)
          let copia = this.state.grupos
          copia.splice(index, 1)
          this.setState({ grupos: copia})
        }}
          >check_circle</i>
    )   
  }

    render() {
        return (
            <div style={{ margin: "10px 15px 10px 15px"}}>
                {/* link a grupo nuevo y buscador por nombre */}
              <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
              <input className="form-control" placeholder="Nombre Grupo"
              value={this.state.busquedaNombreGrupo}
              onKeyPress = { e => {
                if(e.key === 'Enter'){
                  EsquemaGrupo.buscarGrupoPorNombre(this.state.busquedaNombreGrupo)
                  .then( n => {
                  this.setState({grupos: n})
                })}
              }}
              onChange={ e => {this.setState({
                busquedaNombreGrupo: e.target.value
              })}} />
              <i className="material-icons" style={{color:"#0d6e79", fontWeight: "bold"}}
              onClick={ e => {
                EsquemaGrupo.buscarGrupoPorNombre(this.state.busquedaNombreGrupo)
                .then( n => {
                this.setState({ grupos: n})
              })}} >
              search
              </i>{nbsp(3)}
              <Link to = {'/grupo/nuevo'}  style={{ textDecoration: 'none'}}> <span className="decoracion" >Nuevo</span></Link>
              </div>

              <div>
              {this.state.grupos.map(
                (g,i) => {
                  let grupo = Object.assign(new EsquemaGrupo(), g)
                  return(
                    <div key={i} style={{ margin: "10px 15px 10px 15px"}}>
                      <div className="fichaArtGrup">
                      {/* <Link to={`/grupo/${grupo._id}`}>                     
                      <div  className="text-center h3" > {grupo.nombre}</div></Link> */}
                     
                      <Link to={{
                        pathname: `/grupo/${grupo._id}`,
                        desdeBolos: this.props.desdeBolos ? true : false,
                        enviarABolo: this.props.enviarABolo ? this.props.enviarABolo : () => {}
                      }}>                     
                      <div  className="text-center h3" title={grupo.comentario}> {grupo.nombre}</div></Link>
                       {grupo.componentes.map(
                         (a,i)=>{
                           return( <div key={i}>
                             <div className="row">
                             <div className="col-6">
                             <span className="h5" >{a.nombre}:</span>
                             </div>
                            {a.competencias.map((d,i)=>{
                              return(
                              <div key={i}>
                                <div className="col-6">
                               {d.competencia}
                               </div>
                               </div>
                              )
                            })}
                            </div>
                            </div>)
                         }
                       )}
                       {this.props.desdeBolos ? this.renderButtonAnadiABolo(grupo) : <span />}
                       </div>
                      
                      
                    </div>
                    
                  )
                })}
              </div>
                
            </div>
        )
    }
}
