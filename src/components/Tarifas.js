import React, { Component } from 'react'
import {mongo, nbsp, br} from '../index'
import {ObjectID} from 'bson';
import {EsquemaSala} from './Sala';
import { Link } from 'react-router-dom';


export class ModelTarifa {
  constructor(){
    this.concepto ='';
    this.coste= 0;
    this.beneficio= 0;
    this.autor = "5e175bc8c90b4af0eb978734"
  }
}

export class Tarifas extends Component {

  state = { sala: new EsquemaSala(), tarifa: new ModelTarifa() }

  salasMongo = mongo.db('proMusic').collection('salas')

  componentDidMount(){

    this.getOne()
    
  }
  getOne = n =>{
  
    //otra manera de organizar la busqueda de un id concreto
    let idBueno = new ObjectID(this.props.match.params.id)
    this.salasMongo.findOne({_id: idBueno})
    .then( s => {console.log(s); this.setState({sala: s})})
    .catch(e => {console.log(e)})
  }

  borrarTarifa = (i) =>{
    let copia = this.state.sala.tarifas
    copia.splice(i,1)
    this.setState({
      sala: {...this.state.sala, tarifas: copia}
    })

  }

  rellenarInput = (unaTarifa) => {
    console.log(unaTarifa)
    this.setState({tarifa: unaTarifa})
  }

  renderInputTarifas = () => {
    return (
      <div>
        <div className="text-center h2">{this.state.sala.nombre}</div>
         {/* graba en la bbdd */}
         <div className="text-center h2">   
        <i className="material-icons" title="graba"
          style={{color:"#0d6e79"}}
          onClick={ e => {
          let c = this.state.sala;
          this.salasMongo.findOneAndReplace({ _id: new ObjectID(this.props.match.params.id) }, c )
          .then( e => {this.props.history.push(`/riders/${this.state.sala._id}`)})
          .catch(e => console.log(e))
          }}> save </i>
          </div> 
        <hr/><br/> 
      

      <div className="form-inline tarifa">

        <input 
          placeholder="concepto"
          className="form-control" 
          value={this.state.tarifa.concepto}
          onChange={ e =>{this.setState({tarifa:{...this.state.tarifa, concepto:
          e.target.value }})
        }}/>{nbsp(1)}

        <input
        value={this.state.tarifa.coste}
        size="5" 
        placeholder="coste"
        className="form-control"
        type="number"
        onChange={ e => {this.setState({tarifa:{...this.state.tarifa, coste: parseFloat( e.target.value)
         }})}}  />{nbsp(1)}

        <input id="beneficio"
        value={this.state.tarifa.beneficio}
        placeholder="beneficio"
        className="form-control"
        size="5" 
        type="number"
        onChange={ e => {
          this.setState({tarifa: {...this.state.tarifa, beneficio:
            parseFloat( e.target.value) } 
          })}}/>{nbsp(1)}

          {/* añade al array de tarifas */}
      <i className="material-icons" title="añade"
        style={{color:"#753086"}}
        onClick={ e => {
        let copia = this.state.sala.tarifas
        copia.push(this.state.tarifa)
        this.setState({sala:{...this.state.sala, tarifas: copia}})
        this.setState({ tarifa: new ModelTarifa()})
        console.log(this.state.sala)
      }}> check_circle </i> {nbsp(3)}
       


    </div>  

    

      {/* <pre>{JSON.stringify(this.state.sala,undefined,2)}</pre> */}

    </div>    
      )
  }

  render() {

    return (
      <div>

      {/* input tarifas */}
      {this.renderInputTarifas()}{br(2)}

      
      {this.state.sala.tarifas.map((t,i)=>{
        return(
          <ShowTarifa key={i} t={t} id={i} borrar={this.borrarTarifa} rellenar={this.rellenarInput} />
        )})
      } 
     <i className="material-icons" onClick={e => {
        this.props.history.goBack()
      }}>
      fast_rewind
      </i>
      
      </div>
    )
  }
}

class ShowTarifa  extends Component {
  
 render(){
    return( 
      <div className="text-center listado">
        <label style={{color:"#0d6e79", fontWeight: "bold"}}> Concepto </label> : {nbsp(2)}
        <span style={{color:"#753086", fontWeight: "bold"}}> {this.props.t.concepto} </span>{nbsp(5)} 
        <label style={{color:"#0d6e79", fontWeight: "bold"}}> Coste </label> : {nbsp(2)}
        <span style={{color:"#8B0000", fontWeight: "bold"}}> {this.props.t.coste} </span> {nbsp(5)} 
        <label style={{color:"#0d6e79", fontWeight: "bold"}}> Beneficio</label> : {nbsp(2)}
        <span style={{color:"blue", fontWeight: "bold"}}>{this.props.t.beneficio}</span>
            
      {/* borra una fila */}
        <i className="material-icons" 
            title="Borar"
            style={{color:"#8B0000", float:"right"}}
            onClick={ e => { this.props.borrar(this.props.id) }     
            } >delete_forever
        </i>
        {/* edita una fila */}
        <i className="material-icons"
        style={{color:"#0d6e79", float: "right"}} 
        title="editar"
        onClick={()=>{

          this.props.rellenar(this.props.t)
          }} >create</i>
      </div>
    )
  }


}


