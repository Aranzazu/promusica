import React, { Component } from 'react'
import {mongo, stitch,nbsp, br,} from '../index'
import {ObjectID} from 'bson';
import { Link } from 'react-router-dom';
// import { Tarifas } from './Tarifas';

 export class EsquemaSala {
  constructor( ){
    this.nombre ='';//ok
    this.pais=''; //ok
    this.ciudad='';//ok
    this.direccion = '';
    this.contacto ='';//ok
    this.mail= '';//ok
    this.telefono= '';//ok
    this.imagenes= {exterior:'', interior:''};//ok boton la ostia
    this.aforo = 0;//ok
    this.tarifaCancelacion = '';//ok se ve como dato; se mete en tarifas el dato economico si hiciera falta
    this.publicidad = "";//ok el mismo concepto que cancelación
    this.contrato = '';// ok es un pdf; boton la ostia
    this.riderPdf='';//ok es un pdf
    this.rider = [];// ok
    this.parking = { tiene: false, concepto: ''};
    this.caracteristicas='';// ok 
    this.tarifas = [];//ok
    this.autor = "5e175bc8c90b4af0eb978734";// ok
    this.jefes = []
  }
}  

export default class Sala extends Component {

  // componente que mete los datos de la sala en la bbdd para mi

  state = { sala: new EsquemaSala(), insertedId:"", riderFijos:[], newRider:[] }

  salasMongo = mongo.db('proMusic').collection('salas')

  componentDidMount(){

    if (this.props.match.params.id === 'nuevo') {      
      this.setState({
        sala: new EsquemaSala()
      })
    }
    else { this.getOne() }
  }

  getOne = n =>{

    let idBueno = new ObjectID(this.props.match.params.id)
    this.salasMongo.findOne({_id: idBueno})
    .then( s => {console.log(s); this.setState({sala: Object.assign(new EsquemaSala(), s)})})
    .catch(e => {console.log(e)})
  }
  // graba en la bbdd y trae de nuevo esa sala id
  grabaSala =  e => {
    let c = this.state.sala;
    delete c._id;

    this.salasMongo.insertOne(c)
    .then( resp => {            
       this.setState({
        insertedId: resp.insertedId.toString()             
      }, e => { this.props.history.push(`/tarifas/${this.state.insertedId}`)})
      })
      
      
  } 
// inputs del formularío

  rederInputCaracteristicas = () => {
    return(
      <div style={{padding:"15px"}}>
        <textarea
          rows="17"
          value={this.state.sala.caracteristicas}
          placeholder="Caracteristicas y comentaríos Sala"
          className="form-control"
          onChange={e =>{
            this.setState({
              sala:{...this.state.sala, caracteristicas:e.target.value}
            })
          }}

        />
      </div>
    )
  }
  renderInpuTarifaCancelacion = () => {
    return(
      <div>
        <input
        value={this.state.sala.tarifaCancelacion}
        placeholder="gastos cancelacion"
        className="form-control"
        onChange={ e => {
          this.setState({
            sala:{...this.state.sala, tarifaCancelacion: e.target.value }
          })
        }
        }/>
      </div>
    )
  }
  renderInputContrato = () => {
    return(
      <div>
        <input
        value={this.state.sala.contrato}
        placeholder=" Pdf Contrato"
        className="form-control"
        onChange={e => {
          this.setState({
            sala:{...this.state.sala, contrato:e.target.value}
          })
        }}/>
      </div>
    )
  }
  renderInputFotos = () => {
    return (
    <div>
    <input
    value={this.state.sala.imagenes.exterior}
    placeholder="Foto Exterior"
    className="form-control"
    onChange={ e =>{
      this.setState({ 
      sala: {
        ...this.state.sala, 
        imagenes: {...this.state.sala.imagenes, exterior: e.target.value } 
      }
    })}
      
    }/>
    <label> Interior</label> 
    <input
    value={this.state.sala.imagenes.interior}
    placeholder="foto interior"
    className="form-control"
    onChange={e =>{
      this.setState({
        sala:{...this.state.sala,
          imagenes:{...this.state.sala.imagenes, interior: e.target.value}}
      
    })}
      
    }/>
  </div>    
    )
  }
  renderInputPais = () => {
    return (
      <input
      placeholder="pais"
      className="form-control"
      value={this.state.sala.pais}
      onChange={ e =>{this.setState({sala:{...this.state.sala, pais:
        e.target.value }})
    }}/>)
  }
  renderInputCiudad = ()=> {
    return (
    <input
      value={this.state.sala.ciudad}
      placeholder="ciudad"
      className="form-control"
      onChange={ e =>{this.setState({sala:{...this.state.sala, ciudad: e.target.value }})
      }}
       />) 
  }
  renderInputNombre = () => {
    return (
    <input
    value={this.state.sala.nombre}
    placeholder="Nombre sala"
    className="form-control"
    onChange={ e =>{
      this.setState({
        sala:{...this.state.sala, nombre: e.target.value }})
    }}/>)
  }
  renderInputAforo = () => {
    return (
    <input
      value={this.state.sala.aforo}
      placeholder="Aforo"
      className="form-control"
      type="number"
    onChange={ e =>{this.setState({sala:{...this.state.sala, aforo: parseInt(e.target.value )
    }})
  }}/> )
  }
  renderInputDireccion = () => {
    return (
    <input
    value={this.state.sala.direccion}
    placeholder="Dirección"
    className="form-control"
    onChange={ e =>{this.setState({sala:{...this.state.sala, direccion:
    e.target.value }})
  }}/> )
  }
  renderInputContacto = () => {
    return (
    <input
    placeholder="Persona Contacto"
    className="form-control"
    value={this.state.sala.contacto}
    onChange={ e =>{this.setState({sala:{...this.state.sala, contacto:
    e.target.value }})
  }}/>)
  }
  renderInputTelefono = () => {
    return(
      <input
      placeholder="telefono"
      className="form-control"
      value={this.state.sala.telefono}
      onChange={ e =>{this.setState({sala:{...this.state.sala, telefono:
      e.target.value }})
    }}/>
    )
  }
  renderInputMail = () => {
    return(
      <input
      placeholder="mail"
      className="form-control"
      value={this.state.sala.mail}
      type="email"
      onChange={ e =>{this.setState({sala:{...this.state.sala, mail:
      e.target.value }})
   }}/>
    )
  }
  renderPubilicidadSala = () => {
    return(
      <div>
        <input placeholder="Concepto"
        value={this.state.sala.publicidad}
        className="form-control"
        onChange={ e =>{ this.setState({
          sala:{...this.state.sala, publicidad: e.target.value}
        })} }/>   
      </div>
    )
  }
  renderInpuRiderPdf = () => {  
    return(
      <div>
        <input
        value={this.state.sala.riderPdf}
        placeholder="Riders Pdf"
        className="form-control"
        onChange={ e =>{this.setState({sala:{...this.state.sala, riderPdf:e.target.value}})
        }}/>
      </div>  

    )
  }

  renderInputParking = () => {
    return(
      <div className="form-inline">
        <input 
          type="checkbox"
          checked={this.state.sala.parking.tiene}
          onChange={e => {
            this.setState({
              sala: {...this.state.sala,
                parking:{...this.state.sala.parking, tiene: !this.state.sala.parking.tiene }}
            })
          }} 
        />{nbsp(1)}
        <input className="form-control"
        value={this.state.sala.parking.concepto}
        onChange={ e => {
          this.setState({
            sala: {...this.state.sala,
           parking:{...this.state.sala.parking, concepto:e.target.value}}
          });
        }}/>
      </div>
    )
  }


  render() {
   
    // condicional para mostrar u ocultar informacion, boton, link etc..
    let linkTarifas = this.state.sala._id ? <Link to={`/tarifas/${this.state.sala._id}`}> gestionar tarifa </Link> : <span />
    let linkRiders = this.state.sala._id ? <Link to={`/riders/${this.state.sala._id}`}> gestionar riders </Link> : <span />

    let salaCorrecta = this.state.sala.nombre !== '' && this.state.sala.ciudad !== ''      

    return (
      
        <div >
              {/* <pre> {JSON.stringify(this.state.sala, undefined, 2)}</pre>  */}

          <div className="text-center"  >{br(1)}
          
        {/* graba */}        
        <i className="material-icons"
           style={{color: "#753086"}}  
           title="grabar"
           hidden={this.state.sala._id || !salaCorrecta }
            onClick={this.grabaSala}>save</i>{nbsp(2)}

        {/*edita la salas  */}
        <i className="material-icons"
             style={{color:"#753086"}} 
            title="editar"
            hidden={!this.state.sala._id}
            onClick={()=>{
            this.salasMongo.findOneAndReplace({_id:new ObjectID(this.state.sala._id)}, this.state.sala)
            .then (document.getElementById('modal1').style.display = 'block')
          }} >create</i>{nbsp(5)}
          {/* modal ok grabacion alerta tarifas */}
        <div className="modal2" id="modal1">
          <div className="modal2-content">
          <span style={{color:"#0d6e79", fontWeight: "bold"}}> Ok grabado! </span> {br(1)}
          <span style={{color:"#0d6e79", fontWeight: "bold"}}> Quiere editar tarifas o riders? </span> {linkTarifas} {nbsp(1)} {linkRiders}{br(2)}
          <i className="material-icons" title="ver ficha"
          style={{color:"#753086", fontWeight: "bold"}}
          onClick={e => {
            // document.getElementById('modal1').style.display = 'none'
            this.props.history.push(`/fichaSala/${this.state.sala._id}`)
          }}>
            thumb_up
          </i>
          </div>
        </div>  

        {/*icono borra toda la sala de bbdd  */}

        {/* <Link id="linkSalas" to="/listaS" /> */}
        <i className="material-icons" 
            title="Borar"
            style={{color:"#8B0000"}}
            hidden={!this.state.sala._id} 
            onClick={ e =>{
            this.salasMongo.deleteOne({_id:  new ObjectID(this.props.match.params.id)})
            .then( e => {this.props.history.push('/listaS')})
            // .then( () => { document.getElementById('linkSalas').click() })     
            }} >delete_forever
        </i>
          {br(2)}
          <span className="h2" >{this.state.sala.nombre}</span>
        <hr/>
        
      </div>
      {/* formulario datos salas */}
      <div style={{}}>
        <div className="forms">  
          {/* input sala-nombre */}
          <label>Teatro</label>{this.renderInputNombre()}
          {/* input ciuda */}
          <label>Ciudad</label>{this.renderInputCiudad()}
          {/* input direccion */}
          <label>direción</label>{this.renderInputDireccion()}
          {/* input pais */}
          <label>Pais</label>{this.renderInputPais()}          
        </div>

      {/* formulario contacto */}
      <div className="forms forms label" >
        {/* input aforo */}
        <label>Aforo</label>{this.renderInputAforo()} 
        {/* input contacto */}
        <label>Contacto</label>{this.renderInputContacto()} 
        {/* input telefono */}
        <label>Teléfono</label>{this.renderInputTelefono()}
        {/* input mail */}
        <label>e-mail</label>{this.renderInputMail()}     
      </div>
        
      <div className="forms forms label"> 
        {/* input cancelacion string */}
        <label>Cancelación</label>{this.renderInpuTarifaCancelacion()} 
        {/*input publicidad*/}
        <label>Publicidad</label>{this.renderPubilicidadSala()}
        {/* input fotos */}
        <label>Fotos</label>{this.renderInputFotos()}          
      </div>

      <div className="forms forms label">
        {/* input pdfRider */}
        <label>Rider Pdf</label>{this.renderInpuRiderPdf()}
        {/* input contrato */}
        <label>Contrato</label>{this.renderInputContrato()} 
        {/* checbok parking */}
        <label>Parking</label>{this.renderInputParking()}
        {br(1)}
        {linkTarifas} {br(1)}
        {linkRiders}  {br(1)}
      </div>

      </div>
       <div className="text-center">
       <label className="titulo text-center">Caracteristicas</label>
      {this.rederInputCaracteristicas()}
      
      </div>
      <i className="material-icons" onClick={e => {
        this.props.history.goBack()
      }}>
      fast_rewind
      </i>
    
          {/* pre= esta preformateado, no lo pone bonito */}
    </div> 
      
  
    
    )
  }
}


