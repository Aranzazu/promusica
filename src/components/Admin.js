import React, { Component } from 'react'
import {mongo,stitch,nbsp, br,jsonToFile} from '../index'


// ponemos el if para que no cree un credencial anonimo si ya conoce el navegador (dominio) y por direccion IP

// stitch.callFunction('getValue', ['Aran']).then()



export default class Admin extends Component {

  state = { password: '', dom: <div> sin permiso </div>}

  
  artistasMongo = mongo.db('proMusic').collection('artistas')
  bolosMongo = mongo.db('proMusic').collection('bolos')
  espectaculosMongo = mongo.db('proMusic').collection('espectaculos')
  gruposMongo = mongo.db('proMusic').collection('grupos')

  renderAdmin = () =>{
   
   // pasarle el state.password a la funcion adminTrue() de stitch
   stitch.callFunction('adminTrue',[this.state.password]).then(r =>{
    
      this.setState({dom: r ? <ShowAdmin/>: this.state.dom})
     
     
   }) 
  }

  render() {
    return (
      <div className="form-inline ">
        <input className="form-control"
          size="15"
          type = "password"
          value={this.state.password}
          onChange={ e => this.setState({ password: e.target.value}) }
        />

        <i className="material-icons"
        style={{color:"#0d6e79"}} 
        title="añadir a bolo"
        onClick={ this.renderAdmin}  
          // ojo!! no ponemos parentesis ni ponemos un callback por que queremos que ejecute la función cuando se presione el onclick, no necesita mas parametros el onclik va directo a la funcion.
           // este llama al componente de react que pinta el dom
        
          >check_circle
        </i>
        <hr/>
        {this.state.dom}


      </div>
    )
  }
}

class ShowAdmin  extends Component { // pdt de hacer
  state = { salas:[], bolos:[], grupos:[], artistas:[], espectaculos:[]}

render(){
  return(
    <div>
     <button
     className= "btn btn-primary"
     onClick={e =>{
      mongo.db('proMusic').collection('bolos').aggregate([]).toArray()
       .then(b => {this.setState({bolos:b},
         () => {jsonToFile(this.state.bolos, 'bolos.json')})})
     }}
     >c.s. Bolos</button>{nbsp(2)}
      <button
      className= "btn btn-primary"
      onClick={e =>{
        mongo.db('proMusic').collection('artistas').aggregate([]).toArray()
        .then(a => {this.setState({artistas:a},
          () => {jsonToFile(this.state.artistas, 'artistas.json')})})
     }}
     >c.s. Artistas</button>{nbsp(2)}
     <button
     className= "btn btn-primary"
     onClick={e =>{
      mongo.db('proMusic').collection('espectaculos').aggregate([]).toArray()
      .then(e => {this.setState({espectaculos:e},
        () => {jsonToFile(this.state.espectaculos, 'espectaculos.json')})})
     }}
     >c.s. Espectaculos</button>{nbsp(2)}
      <button
      className= "btn btn-primary"
      onClick={e =>{
        mongo.db('proMusic').collection('grupos').aggregate([]).toArray()
        .then(g => {this.setState({grupos:g},
          () => {jsonToFile(this.state.grupos, 'grupos.json')})})
     }}
     >c.s. grupos</button>{nbsp(2)}
      <button
      className= "btn btn-primary"
      onClick={e =>{
       mongo.db('proMusic').collection('salas').aggregate([]).toArray()
       .then(s => {this.setState({salas:s},
         () => {jsonToFile(this.state.salas, 'salas.json')})})
     }}
     >c.s. salas</button>{nbsp(2)}

     
    </div>
  )
}
}
