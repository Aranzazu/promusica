import React, { Component } from 'react';
import {mongo, stitch, nbsp, br,} from '../index';
import { EsquemaSala } from './Sala';
import {ObjectID} from 'bson';




export class RiderSala{
  constructor(){
  this.concepto = '';
  this.comentarios = '';
  this.estado = true;
  this.auto = "5e175bc8c90b4af0eb978734";
  this.jefes = []
  }
}



export class Riders extends Component {
  state = { sala: new EsquemaSala(), riderSala: new RiderSala(), riderFijo: [] } 

  salasMongo = mongo.db('proMusic').collection('salas')

  

  componentDidMount(){
    
     /* esta es la funcion de sticht para traer unos valores fijos */
    stitch.callFunction('getRiderFijo', [])
    .then( r => {
      console.log(r);
      this.setState({riderFijo: r }, () => { this.getOne() })
    }) 
    
  } 

  getOne = n =>{  
    //otra manera de organizar la busqueda de un id concreto
    let idBueno = new ObjectID(this.props.match.params.id)
    this.salasMongo.findOne({_id: idBueno})
    .then( s => {
      console.log(s); 
      this.setState({sala: s}, () => {
      })
    })
    .catch(e => {console.log(e)})
  }

  traeRider = () => {
    let copia = this.state.sala.rider
    this.state.riderFijo.forEach( (rF,i) => {
      console.log(rF)
      let index =  this.state.sala.rider.findIndex( (r,i) => { return rF.concepto === r.concepto} )
      // en la nbusqueda de un array -1 es que no hay conincidencia por lo que queremos que lo suba
      if(index === -1) { 
        copia.push(rF)       
        console.log(copia)
      }       
    })

    this.setState({ sala: {...this.state.sala, rider: copia }}, ()=>{console.log(this.state)})

    
  };

  llenarInput = (unRaider) => {
   this.setState({
     riderSala: unRaider
   })
  }

  borrarUnRider = i => {
    let copia = this.state.sala.rider
    copia.splice(i,1)
    this.setState({
      sala:{...this.state.sala, rider: copia}
    })
  }

  
  renderInputRiders = () => {
    return (
      <div>
        <div className="text-center h2">{this.state.sala.nombre}</div>
      <hr/><br/> 
      

      <div className="form-inline tarifa">

        <button className="btn btn-primary" 
          onClick={e =>{ 
            this.traeRider()
          }}>
          Añadir rider comunes
        </button>

        <input 
          placeholder="concepto"
          className="form-control" 
          value={this.state.riderSala.concepto}
          onChange={ e =>{this.setState({riderSala:{...this.state.riderSala, concepto:
          e.target.value }})
        }}/>{nbsp(1)}

        <input
        value={this.state.riderSala.comentarios}
        placeholder="comentarios"
        className="form-control"
        onChange={ e => {this.setState({riderSala:{...this.state.riderSala, comentarios:
          e.target.value}})}}  />{nbsp(1)}

          {/* añade al array de riders */}
      <i className="material-icons" title="añade"
      style={{color:"#753086"}}
       onClick={ e => {
        let copia = this.state.sala.rider
        copia.push(this.state.riderSala)
        this.setState({sala:{...this.state.sala, rider: copia}})
        this.setState({ riderSala: new RiderSala()})
        console.log(this.state.sala)
      }}> check_circle </i> {nbsp(3)}

        {/* graba en la bbdd */}
      <i className="material-icons" title="graba"
        style={{color:"#0d6e79"}}
        onClick={ e => {
         let c = this.state.sala;
         this.salasMongo.findOneAndReplace({ _id: new ObjectID(this.props.match.params.id) }, c )
         .then( e => {this.props.history.push(`/riders/${this.state.sala._id}`)})
         .catch(e => console.log(e))
         }}> save </i> 

    </div>  



      {/* <pre>{JSON.stringify(this.state.sala,undefined,2)}</pre> */}

    </div>    
      )
  }

  

    render() {
        return (
       
      <div>
        
       {/* input Rider que no viene de stitch */}
       {this.renderInputRiders()}{br(2)}
        {this.state.sala.rider.map((r,i)=>{
        return(
          <ShowRiders key={i} id={i} {...r} borrar={this.borrarUnRider} rellenar={this.llenarInput} />
        )})
      }  
      <i className="material-icons" onClick={e => {
        this.props.history.goBack()
      }}>
      fast_rewind
      </i>
      {/* <pre> {JSON.stringify(this.state.sala, undefined, 2)} </pre> */}

      
      </div>
      
    )
  }
     

}

class ShowRiders  extends Component {
  
  render(){
     return( 
       <div className="text-center listado" onClick={e => {
        //  this.props.pepe(rider)
       }}>

         <label style={{color:"#0d6e79", fontWeight: "bold"}}> Concepto </label> : {nbsp(2)}
         <span style={{color:"#753086", fontWeight: "bold"}}> {this.props.concepto} </span>{nbsp(5)} 
         <label style={{color:"#0d6e79", fontWeight: "bold"}}> comentarios </label> : {nbsp(2)}
         <span style={{color:"#8B0000", fontWeight: "bold"}}> {this.props.comentarios} </span> {nbsp(5)} 
        
          {/* borrar linea */}
         <i className="material-icons" 
            title="Borar"
            style={{color:"#8B0000", float:"right"}}
            onClick={ e => { this.props.borrar(this.props.id) }     
            } >delete_forever
        </i>
         {/* editar linea */}
         <i className="material-icons"
        style={{color:"#0d6e79", float: "right"}} 
        title="editar"
        onClick={()=>{          
          this.props.rellenar( Object.assign(new RiderSala(), this.props) )
          }} >create</i>
       </div>
     )
   }
}   
 
//  este componente tiene que controlar los rider de sala, de rock en familia y los rider de cada espectaculo
// pudiendo enlacarlo : la sala con los rider de la empresa( para ver que nos hace falta ( y especificaciones y particularidades que hay que tener en cuenta a la hora de trabajar en esa sala ejem: la pantalla tiene unas medidas que nos obliga a modificar las diapositivas por que nuestro poryector tiene mas angulo y necesitamos ajustarlas para que se vea completo el video o lo que sea. O hay que llevar cable de no se que, que el suyo no es compatible con el instrumento tal)). Los raider del espectaculo, con los que nosotros tenemos disponibles para ver que necesitamos alquilar. poderselos asignar a un espectaculo (y solo a uno (por horas) para saber con los que contamos para el siguiente espectaculo.). Dudo en si aquí es necesario poner los gastos del alquiler de los rider <i className="material-icons" title="graba" 

