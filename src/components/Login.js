import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {nbsp, br, stitch} from '../index';
import { UserPasswordCredential} from 'mongodb-stitch-browser-sdk';




export  class Login extends Component {
  state = {user:'', password:''}
 
  render() {
    return (

      <div className=" login text-center">
      <div className="logins">
        {/* {stitch.auth.isLoggedIn}  Hola:{stitch.auth.user.profile.email} */}
        <label style={{color:"#572364", fontWeight: "bold",}}>
          USUARIO
        </label>
        <input placeholder="User" 
          className="form-control"
          size="31"
          onChange={ e => {
            this.setState({user: e.target.value})
          }}/>{br(1)}
        <label style={{color:"#572364", fontWeight: "bold"}}>PASSWORD</label><input placeholder="Password" 
          type="password"
          className="form-control"
          onChange={ e => {
            this.setState({password: e.target.value})
          }}/>{br(1)}
{/* Ojo!! revisar este link por que va sin comprobación  */}
          <i class="material-icons" title="login" style={ {color:"#572364"}}
          onClick={()=>{
          stitch.auth.loginWithCredential(new UserPasswordCredential(this.state.user, this.state.password))
          .then( () => {
            let firma = stitch.auth.user.id
            let email = stitch.auth.user.profile.email
             window.location.href= '/' })
        }}>
          touch_app
        </i>{nbsp(5)}

        <i class="material-icons"title="login out" style={{ color:"#0d6e79"}}
          onClick={()=>{
            stitch.auth.logout().then( n => {})
          }}>
          person_add_disabled
        </i>

        {/* {JSON.stringify(this.state)} */}
      </div>
      <div style={{color:"#8B0000", fontWeight: "bold"}}>
      { stitch.auth.isLoggedIn ? '' : 'Debes logearte'}
      </div>
      
      </div>
    )
  }
}
