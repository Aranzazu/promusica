
import React, { Component } from 'react'
import { mongo, stitch, nbsp, br } from '../index'
import { Link, Redirect } from 'react-router-dom';
// import {ObjectID} from 'bson'

// esta clase trae el listado de salas
export class ListadoSala extends Component {
    state = { salas: [], ciudades: [], ciudadActual: '', busquedaNombreSala:'' }

    salasMongo = mongo.db('proMusic').collection('salas')

    getTodasSalas = () => {
      this.salasMongo.aggregate([
        { $sort: { nombre: 1} }
      ]).toArray()     
      .then(s => {
         this.setState({salas: s}) 
         console.log(s)
        })
    }

    changeSelectCiudades = e => {
        this.setState({
          ciudadActual: e.target.value},  
          ()=>{
            if ( this.state.ciudadActual === "todas"){
              this.getTodasSalas()
             }
            else { 
            this.salasMongo.aggregate([
           { $match:{ ciudad: this.state.ciudadActual } },
           {  $sort: {_id: 1} }
          ]).toArray().then(
            (salasCiudad) => { console.log(salasCiudad)
              this.setState({
                salas: salasCiudad
              })
            }
          )}
        })         
    }

    buscarSalaPorNombre = () => {
      let consulta = [ 
        {$match:{ nombre:{$regex: new RegExp(this.state.busquedaNombreSala), $options:'i'}}},
        {$sort:{nombre: 1}}
      ]
  
      this.salasMongo.aggregate(consulta).toArray().then(d => { this.setState({salas: d })})
    }

   componentDidMount(){
     if (stitch.auth.isLoggedIn){
      this.getTodasSalas()
          
        let agrupaCiudad = [
          { $group: {_id: '$ciudad', total: {$sum: 1} } },
          {$sort: {_id: 1}}
        ]

        this.salasMongo.aggregate(agrupaCiudad).toArray().then(
          (ciudades) => { console.log(ciudades)
            this.setState({
              ciudades: ciudades
            })
          })
     }
  } 

 
    
 render() {
  return (
    <div>
  { !stitch.auth.isLoggedIn ? <Redirect to="/login" /> :<div >
  {/* seleccionadores, buscador por nombre y sala Nueva */}
    <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
    <select className="form-control"
      onChange={this.changeSelectCiudades}>
      <option value="todas"> todas las salas </option>
      {this.state.ciudades.map(
        (c,i) => <option key={i} value={c._id}> {c._id} {c.total} </option>
      )}
    </select>{nbsp(2)}

    <input className="form-control" placeholder=" Nombre sala"
      value={this.state.busquedaNombreSala}
      onKeyPress = { e => {
        if(e.key === 'Enter'){this.buscarSalaPorNombre()}
      }}
      onChange={ e => {this.setState({
        busquedaNombreSala: e.target.value
      })}} />

      <i className="material-icons" style={{color:"#0d6e79", fontWeight: "bold"}}
      onClick={ e => {this.buscarSalaPorNombre()}} >
      search
      </i>{nbsp(3)}

    <Link to={'/sala/nuevo'} style={{ textDecoration: 'none'}}> <span className="decoracion" >Nueva</span>  </Link>
    </div>{br(1)}
    {/* listado de salas */}
    <span>
    {this.state.salas.map(
    (s,i) => {
      return(
      <div key={i}> 
        <div  className="ficha" >
        <Link to={`/fichaSala/${s._id}`} style={{textDecoration: 'none'}}> 
        <div className="text-center sombreado">
              <span style={{color:"#753086", fontWeight: "bold",  fontSize: "30px" }}>
              {s.nombre}</span> {br(1)}
              <span className="img.izquierda" style={{color:"#753086", fontWeight: "bold"}}>   
              {s.ciudad}</span>
        </div></Link>
        <Link to={`/sala/${s._id}`} ><span  style={{color:"#0d6e79", fontWeight: "bold", float:"right"}}> 
          <i className="material-icons"  title="editar sala"> create</i></span></Link>
        <div className="text-center" >
          <img src={s.imagenes.exterior} alt="ext"  className=" img.izquierda fotolista"/>
          {nbsp(3)}
          <img src={s.imagenes.interior} alt="int" className="img.derecha fotolista" />
        </div>
        <div>
         
            <span>
              <div className="text-center">
              <span className="img.izquierda" style={{color:"#0d6e79", fontWeight: "bold"}}>
                Aforo: </span>{nbsp(1)}
              <span className="img.izquierda" style={{color:"#753086", fontWeight: "bold"}}>    
              {s.aforo}</span>
              </div>
              <div className="text-center">
                <span className="img.izquierda" style={{color:"#0d6e79", fontWeight: "bold"}}>
                Contacto:</span>{nbsp(1)}
                <span className="img.izquierda" style={{color:"#753086", fontWeight: "bold"}}>   
                {s.contacto}</span>{nbsp(5)}
                <span className="img.izquierda" style={{color:"#0d6e79", fontWeight: "bold"}}>
                Telefono: </span>{nbsp(1)}
                <span className="img.izquierda" style={{color:"#753086", fontWeight: "bold"}}>   
                {s.telefono}</span>{nbsp(5)}
              </div>

              <div className="text-center">
                <span className="img.izquierda" style={{color:"#0d6e79", fontWeight: "bold"}}>
                email:</span>{nbsp(1)}
                <span className="img.izquierda" style={{color:"#753086", fontWeight: "bold"}}>   
                {s.mail}</span>{nbsp(5)}
              </div>  
            </span>
          
       
      </div> 
      </div>
      </div>  
      )
    }
    
  )}
  </span>
   {/* {JSON.stringify(this.state.busquedaNombreSala) } */}
</div> }
</div>
        )
    }
}
