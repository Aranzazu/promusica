import React, { Component } from 'react'
import { mongo, stitch, nbsp, br } from '../index'
import {ObjectID} from 'bson';



export class EsquemaEspectaculo{
  constructor( ){
    this.nombre = ''; // ok
    this.imagen = ''; // ok
    this.video = '';// ok
    this.contabilidad = []; // ok
    this.comentarios = ''; //ok
    this.riderPdf = ''; //ok
    this.rider = []; // ??
    this.autor = "5e175bc8c90b4af0eb978734";// ok
    this.jefes = []
  }
  
  totalCostes(){
    let total = 0;
    this.contabilidad.forEach( c => { 
      total += c.coste  
    });
    return total;
  }
  totalBeneficios(){
    let total = 0;
    this.contabilidad.forEach( c => { 
      total += c.beneficio
    });
    return total;
  }

  // no se si esto esta bien por el return.
  static espectaculosCostes(orden){
    let menorMayor = orden === -1 ? -1 : 1;
    mongo.db('proMusic').collection('espectaculos').aggregate([
      { $unwind: "$contabilidad" },
      { $group: { 
        _id: "$nombre", 
        coste: { $sum: "$contabilidad.coste" },
        beneficio: { $sum: "$contabilidad.beneficio" },
        totalCostes: { $sum: 1 }
      }},
      { $sort: { coste: menorMayor } }
    ]).toArray()
    
  }
}
  


export class ModelContabilidad {
  constructor(){
    this.concepto ='';
    this.coste= 0;
    this.beneficio= 0
  }
  
}

export class RiderEspectaculo{
  constructor(){
  this.concepto = '';
  this.comentarios = '';
  }
}

export class Espectaculo extends Component {

  state = { espectaculo: new EsquemaEspectaculo(), insertedId:'', contabilidad: new ModelContabilidad(), rider: new RiderEspectaculo() }

  mongoEspectaculos = mongo.db('proMusic').collection('espectaculos')

  

  grabaEspectaculo = () => {
    let c = this.state.espectaculo
    delete c._id

    this.mongoEspectaculos.insertOne(c).then( r => {
      this.setState({
        insertedId: r.insertedId.toString()
      })
    })


  }

  getOne = () =>{
    let idBueno = new ObjectID(this.props.match.params.id)
    this.mongoEspectaculos.findOne({_id:idBueno}).then(e => {
      console.log(e); this.setState({
        espectaculo: Object.assign( new EsquemaEspectaculo(), e)
      })
    })

  }

  componentDidMount(){

    if(this.props.match.params.id === 'nuevo'){
      this.setState({ espectaculo: new EsquemaEspectaculo()})
    }
    else { this.getOne()}

  }

  renderInputNombre = () => {
    return(
      <div>
        <input className="form-control"
        placeholder="nombre"
        size = "35"
        // hidden = {this.state.espectaculo.nombre}
        value = {this.state.espectaculo.nombre}
        onChange = { e => {this.setState({
          espectaculo: Object.assign(new EsquemaEspectaculo(), {...this.state.espectaculo, nombre: e.target.value}) 
        })}} />
      </div>
    )
  }
  renderInputPdf = () => {
    return(
      <div>
        <input className = "form-control"
        placeholder = "Pdf rider"
        value = {this.state.espectaculo.riderPdf}
        onChange = { e => {this.setState({
          espectaculo: Object.assign( new EsquemaEspectaculo(), {...this.state.espectaculo, riderPdf: e.target.value})
        })}} />

      </div>
    )
  }
  renderInputImagen = () => {
    return(
      <div>
        <input className="form-control"
        placeholder="imagen"
        size = "5px"
        value = {this.state.espectaculo.imagen}
        onChange = { e => {this.setState({
          espectaculo: Object.assign( new EsquemaEspectaculo(), {...this.state.espectaculo, imagen: e.target.value}) 
        })}} />

      </div>
    )
  }
  renderInputVideo = () => {
    return(
      <div>
        <input className="form-control"
        placeholder="video"
        size = "5px"
        value = {this.state.espectaculo.video}
        onChange = { e => {this.setState({
          espectaculo: Object.assign( new EsquemaEspectaculo(),{...this.state.espectaculo, video: e.target.value})
        })}} />

      </div>
    )
  }
  renderInputcomentarios = () => {
    return(
      <div>
        <textarea className="form-control"
        placeholder="comentarios"
        rows="10"
        value = {this.state.espectaculo.comentarios}
        onChange = { e => {this.setState({
          espectaculo: Object.assign( new EsquemaEspectaculo(), {...this.state.espectaculo, comentarios: e.target.value})
        })}}
        />
      </div>
    )
  }
  renderInputsContabilidad = () => {
    return(
      <div>
          <label className="titulo text-center">Contabilidad:{nbsp(1)}
           <span style={{color: "red"}}>{this.state.espectaculo.totalCostes()}</span>
          </label>
         
          <input 
          placeholder="concepto"
          className="form-control" 
          value={this.state.contabilidad.concepto}
          onChange={ e =>{this.setState({contabilidad:{...this.state.contabilidad, concepto:
          e.target.value }})
        }}/>{nbsp(1)}

        <input
        value={this.state.contabilidad.coste}
        size="5" 
        placeholder="coste"
        className="form-control"
        type="number"
        onChange={ e => {this.setState({contabilidad:{...this.state.contabilidad, coste: parseFloat( e.target.value)
         }})}}  />{nbsp(1)}

        <input id="beneficio"
        value={this.state.contabilidad.beneficio}
        placeholder="beneficio"
        className="form-control"
        size="5" 
        type="number"
        onChange={ e => {
          this.setState({contabilidad: {...this.state.contabilidad, beneficio:
            parseFloat( e.target.value) } 
          })}}/>{nbsp(1)}

          {/* añade al array de tarifas */}
      <i className="material-icons" title="añade"
        style={{color:"#753086"}}
        onClick={ e => {
        let copia = this.state.espectaculo.contabilidad
        copia.push(this.state.contabilidad)
        this.setState({espectaculo: Object.assign(new EsquemaEspectaculo(), {...this.state.espectaculo, contabilidad: copia})})
        this.setState({ contabilidad: new ModelContabilidad()})
        console.log(this.state.espectaculo)
      }}> check_circle </i> {nbsp(3)}

      </div>
    )
  }
  renderInputRider = () => {
    return(
      <div>
        <label className="titulo text-center">Rider</label>
        <input 
          placeholder="concepto"
          className="form-control" 
          value={this.state.rider.concepto}
          onChange={ e =>{this.setState({rider:{...this.state.rider, concepto:
          e.target.value }})
        }}/>{nbsp(1)}

        <input
        value={this.state.rider.comentarios}
        placeholder="comentarios"
        className="form-control"
        onChange={ e => {this.setState({rider:{...this.state.rider, comentarios:
          e.target.value}})}}  />{nbsp(1)}

          {/* añade al array de riders */}
      <i className="material-icons" title="añade"
      style={{color:"#753086"}}
       onClick={ e => {
        let copia = this.state.espectaculo.rider
        copia.push(this.state.rider)
        this.setState({espectaculo:{...this.state.espectaculo, rider: copia}})
        this.setState({ rider: new RiderEspectaculo()})
        console.log(this.state.sala)
      }}> check_circle </i> {nbsp(3)}
      </div>
    )
  }
  renderIconoGrabar = () => {
    return(
      <i className="material-icons"
           style={{color: "#753086"}}  
           title="grabar"
           hidden={this.state.espectaculo._id}
            onClick={this.grabaEspectaculo}>save</i>
    )
  }  
  renderIconoEditar   = () => {
    return(
      <i className="material-icons"
             style={{color:"#753086"}} 
            title="editar"
            hidden={!this.state.espectaculo._id}
            onClick={()=>{
            this.mongoEspectaculos.findOneAndReplace({_id:new ObjectID(this.state.espectaculo._id)}, this.state.espectaculo)
          }} >create</i>
    )
  }
  renderIconoBorrar = () => {
    return(
      <i className="material-icons" 
            title="Borar"
            style={{color:"#8B0000"}}
            hidden={!this.state.espectaculo._id} 
            onClick={ e =>{
            this.mongoEspectaculos.deleteOne({_id:  new ObjectID(this.props.match.params.id)})
            .then( e => {this.props.history.push('/listaE')})
            // .then( () => { document.getElementById('linkSalas').click() })     
            }} >delete_forever
        </i>
    )
  }

  render() {
    
    return (
      <div>
        <div className="text-center" style={{ margin: "10px 15px 10px 15px"}}>
          <div   >
            <span className="h2"> {this.state.espectaculo.nombre} </span>{br(1)}
            {/*grabar la espectaculo  */}
            {this.renderIconoGrabar()}{nbsp(2)}
            {/*edita la espectaculo  */}
            {this.renderIconoEditar()} {nbsp(2)}
            {/*Borra la espectaculo  */}
            {this.renderIconoBorrar()}
          </div><hr/>
          <div className="form-inline" style={{ margin: "0px 250px 0px 250px"}}>
            {this.renderInputNombre()} {nbsp(2)} 
            {this.renderInputImagen()} {nbsp(2)}  
            {this.renderInputVideo() } {nbsp(2)} 
            {this.renderInputPdf()   } {nbsp(2)}  
            <a href={this.state.espectaculo.riderPdf} target="_blank" className="titulo" rel="noopener noreferrer">Rider PDF</a>

          </div>{br(1)}

          <div className="text-center">
            <label className="titulo text-center">Comentarios</label>
            {this.renderInputcomentarios()}
          </div>
            {/* input contabilidad y su listado */}
          <div className="form-inline" style={{ margin: ""}}>
            {/* contavilidad */}
            <div className="listaTarifas">
              {this.renderInputsContabilidad()}
              {br(1)}
                <div className = "listado">          
              {this.state.espectaculo.contabilidad.map(
              (c,i)=>{
                return(
                  <div key={i} className = "listado form-inline">
                    <label style={{color:"#0d6e79", fontWeight: "bold"}}> Concepto </label> : {nbsp(2)}
                    <span style={{color:"#753086", fontWeight: "bold"}}> {c.concepto} </span>{nbsp(5)} 
                    <label style={{color:"#0d6e79", fontWeight: "bold"}}> Coste </label> : {nbsp(2)}
                    <span style={{color:"#8B0000", fontWeight: "bold"}}> {c.coste} </span> {nbsp(5)} 
                    <label style={{color:"#0d6e79", fontWeight: "bold"}}> Beneficio</label> : {nbsp(2)}
                    <span style={{color:"blue", fontWeight: "bold"}}>{c.beneficio}</span>
                        
                  {/* borra una fila */}
                    <i className="material-icons" 
                        title="Borrar"
                        style={{color:"#8B0000", float:"right"}}
                        onClick={ e => {
                          let copia = this.state.espectaculo.contabilidad
                          copia.splice(i, 1)
                          this.setState({ espectaculo: {...this.state.espectaculo, contabilidad: copia}})
                        }     
                        } >delete_forever
                    </i>
                    {/* edita una fila */}
                    <i className="material-icons"
                    style={{color:"#0d6e79", float: "right"}} 
                    title="editar"
                    onClick={()=>{
                      (this.setState({contabilidad: c}))
                      }} >create</i>
                            </div>
                          )
                        }
                      )}
            </div>
            </div> 
      
            {/* rider */}
            <div className="listaTarifas">
              {this.renderInputRider()}
              {br(1)}
                <div className = "listado">          
              {this.state.espectaculo.rider.map(
              (c,i)=>{
                return(
                  <div key={i} className = "listado form-inline">
                    <label style={{color:"#0d6e79", fontWeight: "bold"}}> Concepto </label> : {nbsp(2)}
                    <span style={{color:"#753086", fontWeight: "bold"}}> {c.concepto} </span>{nbsp(5)} 
                    <label style={{color:"#0d6e79", fontWeight: "bold"}}> Comentarios </label> : {nbsp(2)}
                    <span style={{color:"#8B0000", fontWeight: "bold"}}> {c.comentarios} </span> {nbsp(5)}       
                  {/* borra una fila */}
                 
                    <i className="material-icons" 
                        title="Borrar"
                        style={{color:"#8B0000"}}
                        onClick={ e => {
                          let copia = this.state.espectaculo.rider
                          copia.splice(i, 1)
                          this.setState({ espectaculo: {...this.state.espectaculo, rider: copia}})
                        }     
                        } >delete_forever
                    </i>
                    {/* edita una fila */}
                    <i className="material-icons"
                    style={{color:"#0d6e79"}} 
                    title="editar"
                    onClick={()=>{
                      (this.setState({rider: c}))
                      }} >create</i>
                
                  </div>
                          )
                        }
                      )}
            </div>
            </div>    
          </div>
                
        </div>
       <pre> { JSON.stringify(this.state.espectaculo, undefined,2)}</pre> 
      </div>
    )
  }
}

class Contabilidad extends Component {
  
}





