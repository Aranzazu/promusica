import React, { Component, useImperativeHandle } from 'react';
import { mongo, stitch, nbsp, br, Aran } from '../index'
import { Tarifas } from './Tarifas';
import { Espectaculo } from './Espectaculo';
import { ListadoSala } from './ListadoSala';
import {EsquemaSala} from './Sala';
import { Link } from 'react-router-dom';
import { EsquemaEspectaculo } from './Espectaculo';
import ListadoEspectaculos from './ListadoEspectaculos';
import ListadoGrupo from './ListadoGrupo';
import { EsquemaGrupo } from './Grupo';
import ListadoArtistas from './ListadoArtistas';
import {ObjectID} from 'bson';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';







export class EsquemaBolo {
  constructor(){
    this.nombre = ""; //ok
    this.fecha = ""; //ok
    this.imagen = ""; //ok
    this.sala = {}; //ok
    this.tarifas = [];
    this.grupos = []; //ok
    this.espectaculos = [];//ok
    this.entradas = [];
    this.roturadeVentas = [];
    this.importeMinRentable = 0;
    this.produccion = [];
    this.publicidad = [];
    this.autor = "5e175bc8c90b4af0eb978734";
    this.jefes = []
  }
  insertOneToDb() {
      return stitch.callFunction('getValue', ['conf']).then(conf =>{
      this.autor = conf.autor
      this.jefes = conf.jefes
      return mongo.db('proMusic').collection('bolos').insertOne(this)
    } )
   
   }
   findOneAndReplaceToDb(id){
    return stitch.callFunction('getValue', ['Aran']).then(conf =>{
      this.autor = conf.autor
      this.jefes = conf.jefes
    return mongo.db('proMusic').collection('bolos').findOneAndReplace({_id: new ObjectID( id )}, this)
    }) 
    }
   deleteOneToDb(id){
    return mongo.db('proMusic').collection('bolos').deleteOne({_id: new ObjectID(id)})
   } 

  fechaOrden(){
    return this.fecha.split('-')[2] + '-' +
    this.fecha.split('-')[1] + '-' +
    this.fecha.split('-')[0]  
  }

  /*  busquedaPorDia(){
    return mongo.db('proMusic').collection('bolos').aggregate([
      { $match : { fecha: {$eq: this.state.bolo.dia}}}
    ]).toArray()
   }  */ 
  static busquedaPorFecha(d,h){
    console.log(d,h)
    return mongo.db('proMusic').collection('bolos').aggregate([
      {$match: {fecha: {$gte: d,  $lte: h } }},
      {$sort: {fecha: 1, nombre: 1}}
    ]).toArray()
   } 
}

class EsquemaEntrada {
   constructor(){
     this.tipo = "";
     this.precio = 0;
     this.vendidas = 0;
     this.devueltas = 0;
   }
}

export default class Bolo extends Component {
  mongoBolo = mongo.db('proMusic').collection('bolos')

  state = { bolo: new EsquemaBolo(), 
            entrada:new EsquemaEntrada(), 
            salas:[],
            ciudadActual:'',
            busquedaNombreSala:'',
            ciudades:[],
            grupoSeleccionado: new EsquemaGrupo(),
            ocultarSalas: true,
            ocultarRoster: true,
            ocultarGrupo: true,
            fechaDesde:'',
            fechaHasta:''
          }

 

  componentDidMount(){
    this.getTodasSalas()
    this.aggregateCiudadSala()
    if (this.props.match.params.id === 'nuevo') {      
      this.setState({
       bolo: new EsquemaBolo()
      })
    }
    else { this.getOne() }
  }
  getOne = n =>{

    let idBueno = new ObjectID(this.props.match.params.id)
    this.mongoBolo.findOne({_id: idBueno})
    .then( b => {console.log(b); this.setState({bolo: Object.assign(new EsquemaBolo(), b)})})
    .catch(e => {console.log(e)})
  }
 
  

  enviarABolo = (str, obj) => {
    let copia = this.state.bolo[str]
    copia.push(obj)
    this.setState({
      bolo:Object.assign(new EsquemaBolo(),  {...this.state.bolo, [str]: copia})
    })
  }
  enviarAGrupo = (art) => {
    let copia= this.state.grupoSeleccionado.componentes
    copia.push(art)
    this.setState({
      grupoSeleccionado:{...this.state.grupoSeleccionado, componentes: copia}
    })
  }
  

  getTodasSalas = () => {
    mongo.db('proMusic').collection('salas').aggregate([
      { $sort: { nombre: 1} }
    ]).toArray()     
    .then(s => {
       this.setState({salas: s}) 
       console.log(s)
      })
  }
  aggregateCiudadSala = () => {
    mongo.db('proMusic').collection('salas').aggregate([
      { $group: {_id: '$ciudad', total: {$sum: 1} } },
      {$sort: {_id: 1}}
    ]).toArray().then(
      (ciudades) => { console.log(ciudades)
        this.setState({
          ciudades: ciudades
        })
      })
  }
  changeSelectCiudades = e => {
    this.setState({
      ciudadActual: e.target.value},  
      ()=>{
        if ( this.state.ciudadActual === "todas"){
          this.getTodasSalas()
         }
        else { 
          mongo.db('proMusic').collection('salas').aggregate([
       { $match:{ ciudad: this.state.ciudadActual } },
       {  $sort: {_id: 1} }
      ]).toArray().then(
        (salasCiudad) => { console.log(salasCiudad)
          this.setState({
            salas: salasCiudad
          })
        }
      )}
    })         
  }

  buscarSalaPorNombre = () => {
  let consulta = [ 
    {$match:{ nombre:{$regex: new RegExp(this.state.busquedaNombreSala), $options:'i'}}},
    {$sort:{nombre: 1}}
  ]

  mongo.db('proMusic').collection('salas').aggregate(consulta).toArray().then(d => { this.setState({salas: d })})
  }
  anadirSalaABolo = (s) => {
    
    this.setState( {
      bolo: Object.assign(new EsquemaBolo(), {...this.state.bolo, sala: Object.assign(new EsquemaSala(), s) }),
      salas:[],
      ocultarSalas:true

    }) 
  }

  renderSalas = () =>{
    return(
      <div>
         <div className="form-inline" >
        <select className="form-control"
          onChange={this.changeSelectCiudades}>
        <option value="todas"> todas las salas </option>
        {this.state.ciudades.map(
          (c,i) => <option key={i} value={c._id}> {c._id} {c.total} </option>
        )}
        </select>{nbsp(2)}

        <input className="form-control" placeholder=" Nombre sala"
          value={this.state.busquedaNombreSala}
          onKeyPress = { e => {
            if(e.key === 'Enter'){this.buscarSalaPorNombre()}
          }}
          onChange={ e => {this.setState({
            busquedaNombreSala: e.target.value
        })}} />

        <i className="material-icons" style={{color:"#0d6e79", fontWeight: "bold"}}
          onClick={ e => {this.buscarSalaPorNombre()}} >
          search
        </i>{nbsp(3)}
      </div>
        {this.state.salas.map((s,i) => <SalaBolo key={i} {...s} anadirSalaABolo={this.anadirSalaABolo}/>)} 

      </div>
    )
  }
  renderInputImagen = () => {
    return (
    <div>
       <input
    value={this.state.bolo.imagene}
    placeholder="Imagen Promo"
    className="form-control"
    onChange={ e =>{
      this.setState({ 
       bolo: Object.assign(new EsquemaBolo(),{...this.state.bolo, imagen:e.target.value})} 
       )}}
    />
    </div>
    )
  }
  renderInputFechaNombre = () => {
    return(
      <div className="form-inline">
        <input className="form-control"
         placeholder="Nombre Bolo"
        value={this.state.bolo.nombre}
        onChange={ e => {this.setState({
          bolo: Object.assign(new EsquemaBolo(), {...this.state.bolo, nombre: e.target.value})
        })}} />{nbsp(2)}
        <input type="date"
          className="form-control"
          value={this.state.bolo.fecha}
          onChange={ e => {this.setState({
            bolo: Object.assign(new EsquemaBolo(),{...this.state.bolo, fecha:e.target.value})
          })}} />
      </div>
    )
  }
//esto pinta los grupos que tiene el bolo
  rederMapGrupoBolo = () => {
    return(
      <div>
        {this.state.bolo.grupos.map( (g,i) => {
          let artistas = g.componentes ? g.componentes : []
          return(
            <div key={i}  style={{ margin: "10px 15px 10px 15px"}}>
              <Card  className="text-center" style={{ width: '18rem' }}>
              <Card.Header><div className="h3 text-center sombreado"
                  title="editar"
                  onClick= { e => {
                    this.setState({
                      grupoSeleccionado: this.state.bolo.grupos[i]
                    })
                   document.getElementById("modalGrupo").style.display = 'block'
                  } }
              >
                {g.nombre}
             </div></Card.Header>
              <div  style={{ margin: "10px 15px 10px 15px"}} > 
              {/* viene de la variable g.artistas */}
              {artistas.map( (a,i2)=>{
                return(
                  <div key={i2} >
                    <Card.Body>
                <Card.Title>
                <div  className="row">
                      <div className="col-10">
                        {a.nombre}
                      </div>
                      <div className="col-2">
                      <i className="material-icons" 
                          title="Borrar artista"
                          style={{color:"#8B0000"}}
                          onClick={ e =>{
                            artistas.splice(i2,1)
                            this.state.bolo.grupos[i].artistas = artistas
                            this.setState({ bolo:Object.assign( new EsquemaBolo(), {...this.state.bolo, [this.state.bolo.grupos[i]]: artistas }) })
                          }} >
                        delete_forever
                      </i> 
                      </div>
                    </div>
                </Card.Title>
                    <div>
                    {a.competencias.map( (c,i3)=>{
                    return(
                    <div key={i3}  className="row" >
                       
                      <div className="col-10">
                      {/* <Card.Subtitle className="mb-2 text-muted"> */}
                      {c.competencia}, {nbsp(2)} {c.cache}€
                      {/* </Card.Subtitle>  */}
                      </div>
                      <div className="col-2">
                      <i className="material-icons" 
                        title="Borrar competencia"
                        style={{color:"#8B0000"}}
                        onClick={ e =>{
                          a.competencias.splice(i3,1)
                          this.setState({bolo:Object.assign( new EsquemaBolo(),{...this.state.bolo, [this.state.bolo.grupos[i].componentes[i2].competencias]: a.competencias})})
                        }} >
                      clear
                    </i>
                    </div>
                    
                    </div>)
                    })} 
                  </div>
                  </Card.Body>
                  </div>
                )
              })} 
            </div> 
            {this.renderModal()}
        </Card>
          </div> 
       
          )
        })}
       
      </div>
    )
  }  
  renderModal = () => {
    return(
      <div className="modalBolo" id="modalGrupo" onClick={e => {
        if (e.target.id === "modalGrupo") { 
          document.getElementById('modalGrupo').style.display = 'none'
        }
      }}>
  <div className="modalBolo-content ">
    <div className=" h3 text-center">
    {this.state.grupoSeleccionado.nombre}{nbsp(5)}
    <i className="material-icons"
          title="guardar en Bolo"
          onClick={e => {
            // lo que buscamos es el indice y lo que sabemos es el _id
            let index = this.state.bolo.grupos.findIndex( (g,i)=>{
               return g._id === this.state.grupoSeleccionado._id 
              });
            this.setState({
              bolo:Object.assign( new EsquemaBolo(),{ ...this.state.bolo, [this.state.bolo.grupos[index]]: this.state.grupoSeleccionado })
            }, () => console.log(this.state.bolo.grupos))
            document.getElementById("modalGrupo").style.display = "none"
            
          }}>
          done_outline
          </i>
      
    </div>
    {this.state.grupoSeleccionado.componentes.map(
      (a,i)=>{return(
      <div key={i} className="row">
        <div className="col-6">
        {a.nombre}
        </div>
        <div className="col-6" >
        {a.competencias.map( (c,i2)=>{
          return(
            <div key={i2} className="">
        {c.competencia}:{nbsp(2)}{c.cache}€
              <i className="material-icons" 
                        title="Borrar competencia"
                        style={{color:"#8B0000"}}
                        onClick={ e =>{
                          a.competencias.splice(i2,1)
                          this.setState({grupoSeleccionado:{...this.state.grupoSeleccionado, [this.state.grupoSeleccionado.componentes[i].competencias[i2]]: a.competencias}})
                        }} >
                      clear
                    </i>
            </div>
          )
        })}
        </div>
       
      </div>
      )}
    )}
    <hr />
     <ListadoArtistas desdeBolos={true} enviarAGrupo={this.enviarAGrupo}/>
  </div>
      </div>
    )
  }
  renderIconoGrabar = () => {
    return(
      <i className="material-icons"
           style={{color: "#753086"}}  
           title="grabar"
           hidden={!this.props.match.params.id}
             onClick={() => {
              this.state.bolo.insertOneToDb()
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }}>save</i>
    )
  } 
  renderIconoEditar   = () => {
    return(
      <i className="material-icons"
             style={{color:"#753086"}} 
            title="editar"
            hidden={!this.props.match.params.id}
            onClick={ () => { this.state.bolo.findOneAndReplaceToDb(this.props.match.params.id)
              .then( d => {console.log( d )})
              .catch( e => {console.log( e )})} 
            } >create</i>
    )
  }
  renderIconoBorrar = () => {
    return(
      <i className="material-icons" 
            title="Borrar"
            style={{color:"#8B0000"}}
            hidden={!this.props.match.params.id} 
            onClick={ e =>{this.state.bolo.deleteOneToDb(this.props.match.params.id)
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }} >
          delete_forever
        </i>
    )
  }
 

  render() {
    return (
      <div style={{ margin: "10px 15px 10px 15px"}} >
       <div className="text-center">
        {this.renderIconoGrabar()}{nbsp(2)}
        {this.renderIconoEditar()}{nbsp(2)}
        {this.renderIconoBorrar()}
        </div> <hr/>
        

        <div className="form-inline">
           {this.renderInputFechaNombre()}{nbsp(2)}  
           {this.renderInputImagen()}{nbsp(2)}      
        <div 
          onClick={e => {this.getTodasSalas(); this.setState({ocultarSalas:
          !this.state.ocultarSalas})}}
          className="decoracion"
        >SALAS</div>{nbsp(2)}
        <div className="decoracion"
         onClick={ e => {this.setState({ocultarRoster:!this.state.ocultarRoster})}}>
          ROSTER
        </div>{nbsp(2)}
        <div className="decoracion"
         onClick={ e => {this.setState({ocultarGrupo:!this.state.ocultarGrupo})}}>
          GRUPOS
        </div>

        </div>{br(1)}
        <div hidden={this.state.ocultarSalas}>
          {this.renderSalas()}
        </div>
      {/* pintamos en el dom los datos del bolo. */} 
        {this.state.bolo.sala.nombre}:{nbsp(2)}{this.state.bolo.sala.ciudad} ({this.state.bolo.sala.aforo}{nbsp(2)}Aforo)
        {this.state.bolo.espectaculos.map((e,i)=>{
          return (<div key={i}>
              {e.nombre}
          </div>)
        })}
        {this.rederMapGrupoBolo()}
        
     
        
        
        <div hidden={this.state.ocultarRoster}>
          <ListadoEspectaculos desdeBolos={true} enviarABolo={this.enviarABolo} />
        </div>
         <div hidden={this.state.ocultarGrupo}>
           <hr />
          <ListadoGrupo desdeBolos={true} enviarABolo={this.enviarABolo} />
          {br(1)}
          <div>
         
        </div>
        </div>
       
          <pre> { JSON.stringify(this.state, undefined,2)}</pre>
      </div>
    )
  }
}



export class SalaBolo extends Component {
  state = { sala: Object.assign(new EsquemaSala(), this.props)}
  
  renderButtonAnadiABolo = () =>{

    return(
      <i className="material-icons"
        style={{color:"#0d6e79"}} 
        title="añadir a bolo"
        onClick={ e => { 
          this.props.anadirSalaABolo(this.props) 
        }}
         >check_circle</i>
    )  
  } 

  render() {

    return (
      <div className="ficha">
        <div className="text-center">
        <Link to={`/fichaSala/${this.props._id}`} style={{textDecoration: 'none'}} target="_blank"> {this.props.nombre}</Link>{nbsp(2)}
        {this.renderButtonAnadiABolo()}
        {br(1)}
       {this.props.ciudad}{nbsp(1)} ({this.props.aforo}{nbsp(1)}Aforo)
       </div>  
       <div className="row">
         <div className="col-6">
       <u>Riders:</u>
        {this.state.sala.rider.map((r,i)=>{return(
          <div key={i}> 
          {r.concepto}
          {r.comentario}
          </div>
        )})}
        </div>
        <div className="col-6">
        <u>Tarifas:</u>
        {this.state.sala.tarifas.map((t,i)=>{
          return(
            <div key={i} className="row">
              <div className="col-10">
                {t.concepto}
                {t.coste}
                {t.beneficio}
              </div>
              <div className="col-2">
                <i className="material-icons" 
                  title="Borrar"
                  style={{color:"#8B0000", float:"right"}}
                    onClick={ e => { 
                      let copia = this.state.sala.tarifas
                      copia.splice(i,1)
                      this.setState({ 
                        sala: Object.assign( new EsquemaSala(), {...this.state.sala, tarifas: copia})  
                      })
                    }}>clear
                </i> 
              </div>
            </div>
          )
        })}
        </div>
       </div>  
      </div>
    )
  }
}
export class BoloGrupoArtista extends Component{
  state = { grupo: Object.assign(new EsquemaGrupo(), this.props)}

  renderIconoBorrar = () => {
    return(
      <i className="material-icons" 
            title="Borrar"
            style={{color:"#8B0000"}}
            onClick={ e =>{this.props.splice(this.props)
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }} >
          delete_forever
        </i>
    )
  }
  render(){
    return(
      <div>
        {this.props.componentes.map(
          (a,i)=>{return(
            <div key={i}>
              {a.nombre}
              {a.competencia}
              {a.cache}
              {a.renderIconoBorrar()}

            </div>
          )}
        )}
      </div>
    )
  }
}
