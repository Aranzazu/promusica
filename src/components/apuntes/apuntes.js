import React, { Component } from 'react'
import Speech from 'speak-tss'// libreria parea que te lea el ordenador y hau que bajarsela de node el la spaeak-tss
import{link, Redirect} from 'react-router-dom'
import { mongo } from '../..'
import { BrowserFetchTransport } from 'mongodb-stitch-browser-sdk'

 // agregate, para traer solo un atributo de una coleccion en la consola de mongo que lo puedo pasar para react db.bolo.aggregate([ {$match: {video:{$ne: ""(ne= no tenga "" un string bacio), $exists:true}}}, {$proyect:{}}]).toArray() ejem: mongo.db("proMusic").collection("bolo").aggregate([])


// setInterval(), setTimeout( ) es con javascript
//setTimeout(cb, tiempo ) el primer parametro es una colback y el segundo el tiempo en milisegundos

// checkbox: con react
state={ cumplido:false}

carteleria: <input id="carteles"  
type="checkbox" 
checked={this.state.bolo.publicidad[i].cumplido}
onChange={ e =>{this.setState({cumplido: !this.state.cumplido})}} />

// en mi caso  por que va dentro de un map
render(){
  return(
    <div>
      {this.state.bolo.publicidad.map((p, i) => {
        return(
          <div key={i}>
            {p.concepto}
            <input
              checked={this.state.bolo.publicidad[i].cumplido}
              onChange={ e =>{
                let copia = this.state.bolo.publicidad
                copia[i].cumplido = !copia[i].cumplido
                this.setState({ bolo: {...this.state.bolo, publicidad:copia}})
              }}
              type="checkbox"/> 

          </div>
        )
      })

      }
    </div>
  )
}

// checkbox: con javascript
bbdd viene = tarea = { concepto:"carteleria", cumplida: false}

carteleria: <input id="carteles"  
type="checkbox" 
checked="true" />

<script>
  function Grabar(){
    tare.cumplida = document.getElementById('carteles').checked;
  }

function Grabar(){
    tare.cumplida = document.getElementById('carteles').checked = tarea.cumplida;
  }


</script>
// reactradio, ponemos el atributo name por que el id no se puede repetir
<input type="radio" 
name="vehiculo"
 value="furgo grande" 
 checked ={this.state.bolo.vehiculo === 'furgo grande'}
 onChange={ e => {this.setState({
  bolo: {...this.state.bolo, vehiculo: e.target.value}
})}}/>
<input type="radio"
 name="vehiculo"
  value="furgo pequeña"
  checked ={this.state.bolo.vehiculo === 'furgo pequeña'}
 onChange={ e => {this.setState({
  bolo: {...this.state.bolo, vehiculo: e.target.value}/>
<input type="radio"
 name="vehiculo"
 value="coche"
 checked ={this.state.bolo.vehiculo === 'coche'}
 onChange={ e => {this.setState({
  bolo: {...this.state.bolo, vehiculo: e.target.value}/>

// jquery radio, ponemos el atributo name por que el id no se puede repetir
<input type="radio" name="vehiculo" value="furgo grande"/>
<input type="radio" name="vehiculo" value="furgo pequeña"/>
<input type="radio" name="vehiculo" value="coche"/>

<script>
let bolo = { vehiculo: $('input[name="vehiculo"]:checked').val()}
// esto seria lo que se guardaria en la bbdd con los :checked nos selecciona el checado del dom.
 
$(`input[name="vehiculo"][value="${bolo2.vehiculo}"]`). prop('checked', true) // seleccionamos el input que esta checado y con la prop cambamos el contenido(propiedad) de un atributo de html
</script>


state= { ocultarMensaje: true}

render(){
  return(
    <div>
      <button onClick={() =>{
        this.setState({ocultarMensaje:false})
        setTimeout( ()=>{
          this.setState({ocultarMensaje:true})
        }, 1000)
      }}>grabar</button>

      <div hidden={this.state.ocultarMensaje}> Ok grabado</div>
    </div>
  )
}

// setInterval(), se suele utilizar para actualizar los listados, o para que se grabe algo cada x tiempo.

state= { ocultarMensaje: true}

render(){
  return(
    <div>
      <button onClick={() =>{
       
      let x =  setInterval( ()=>{
         /*  let copia = this.state.datos
          copia.push(new Date) */
          // tambien podemos subir datos a un array de la manera de abajo y es jabascript, lo podemos utilizar en cualquier momento 
          this.setState({datos:[...this.state.datos, new Datos]})
        },3000)
      }}>grabar</button>

      <div hidden={this.state.ocultarMensaje}> Ok grabado</div>
    </div>
  )
}
// esto lo utilizamos para cerra el setInterval
<button onClick ={e => clearInterval( this.x)}></button>  
// ejemplo de un while( loop = bucle)

export function nbsp(n) {
  let ret = []
  let index = 1
  while ( index<=n){
    ret.push(<span key={index}>&nbsp;</span>)
    index++
  }
  return ret
}

// es otra manera de ir a una pagina, pero cuando llega a leerlo el programa directamente te cambia de pagina y deja de leer el resto de las lineas, funciona con el estate ejem:
state = { cambioPagina: false}

{this.state.cambioPagina ?<Redirect  to="/sala"/>: <span/>} 

<button onClick={ e => {
  this.setState({ cambioPagina:true})
}}></button>


// css = el z.index: 1 le estamos diciendo que esta por debajo y so le ponemos -1 que esta por encima 

// como hacer un camnbio de pagina sin el link
<button onClick={e => {
  this.props.history.push('/y la ruta que queramos')
}}>cambiar a otro componente</button>

//esto se utiliza para ir hacia atras y yo lo he puesto dentro de un icono.
this.props.history.goback()

// funcion de fetch(el ajax de java escrip puro)
//ojo!! lo que esta dentro del headers es meta informacion y siempre que sea jeson va así, si mandamos html o xml hay que biscar como escribirlo.

export const ajaxGet =( url, cb) => {
  fetch(url, {
    method: 'GET',
    headers:{'Accept: 'application/json','Content-type':'application/json'},
    body: JSON.stringify(this.state)
  })
  .then()
  .then()
}

// otra forma de hacer el ajax con una funcion asincrona y con el await es como hacer el fetch pero lo metemos dentro de la funcion por eso no tiene promise osea then(). el await es para que espere hasta que le llegue la informacion y solo funciona cuando le decimos que la funcion es async
export async function ejemploFuncionAjax(url, cb){
  let headers:{'Accept: 'application/json','Content-type':'application/json'}
  let method:'GET'
  let response: await fetch(url, {method:method, headers:headers})
  let data: await response.json()
  cb(data)
}



// funcion split()cambia un string en un array indicandole el corte ejmp:
//toISOSstring() cambia el formato de una fecha al strig
fecha1 = new Date().toISOString().substring(0,10)
fecha2 = new Date().toISOString().split(T)[0]// todas las fechas van cortadas con una T y lo que queremos que nos muestre es el indice 0 

//botones para ajax y para que vuelva al input, tambien para que cuando des al retun haga la misma funcion que el boton
class esquemaSala {
  constructor(){
  this.riderFijo = {
    mesaMezcla:false, tecnico:false, genteSala:false, proyector:false, pantalla:false, tecnicoCabinaControl:false
    }
  }
}
export default class Sala extends Component {
  state = {}
  renderCheckMesamezcla = e => {
    return (
    <input id="mesaMezcla"
        type="checkbox"
        onChange={this.checkedRider}
        placeholder="rider"/>
        
        ) 
  }
  reder(){
    return(
      <div>
        Mesa mezcla:{this.renderCheckMesamezcla()}
      </div>
    )
  }
    
}

// pone o quita los check del rider
checkedRider = (e) => {
  this.setState({
    sala: {...this.state.sala,
    riderFijo: {...this.state.sala.riderFijo,
    [e.target.id]: !this.state.sala.riderFijo[e.target.id]}}
    //[e.target.id]es como le decimos a ES6 que coja el valor de la variable id ya que es un string
  })  

export default class Test extends Component {
  state = { local:"" }

  botonGuardar = () =>{
    return(
    <button 
          className="btn btn-primary"
          onClick={ e => {
             console.log(this.state);
             this.setState({local:''});
             document.getElementById('local').focus()//CUANDO PINCHAS EL BOTON EL CURSOR VUELVE AL INPUT
             } }> Guardar </button>
    )         
  }

  render() {
    let css = {padding: 20}

    return (

      <div style={css} >
        Local: {this.state.local}

        <input 
          className="form-control"
          placeholder="escribe local"
          id="local"
          value={this.state.local} //para que pinte el valor que le hemos dado al state//
          onKeyDown= {  e=> {
            console.log(e.key)
            if (e.key==='Enter') {this.setState({local:''})}
          }}//kay= es el nombre de la tecla dentro de e(evento)}
          onChange={e => { 
            this.setState({ local: e.target.value})
          }}
        />
        <br/>
        
       { this.botonGuardar()}   
       {JSON.stringify( this.state)}
      </div>
    )
  }
}

// como funcionan los checbox e input para formularios basicos, ponemos los ... para destructurar el objeto y que nos deje modificar solo un atributo del mismo sin borrar el resto.
import React, { Component } from 'react'

export default class Test extends Component {
  state = {
    averia: { fecha:'', confirmada: false, precio: 0, sistema: ''}
    }
  render() {
   return (
     <div>
       <input
       value={this.state.averia.fecha}
       onChange={e => {
         this.setState({averia: {...this.state.averia, fecha: e.target.value } })}}
       type="date" />
  
        confirmada:
         <input
         checked={this.state.averia.confirmada}
         onChange={e => this.setState({ averia: {...this.state.averia, confirmada:!this.state.averia.confirmada}})}
         type="checkbox" />

       
      <input type="number"
      placeholder="valor" min="1" max="10" step="0,5"
      onChange={ e =>{
        this.setState({ averia: {...this.state.averia, precio: e.target.value}})
      }} 
      value={this.state.averia.precio}
      /> 
      <select onChange={e => {
        this.setState({ averia: 
          {...this.state.averia, 
          sistema: e.target.value}})
      }}>
        <option value="linux">linux</option>
        <option value="osx">osx</option>
        <option value="windows">windows</option>
        </select> 
{/* esta es la funcion de sticht para traer unos valores fijos
        stitch.callFunction('getRiderFijo', [])
    .then( r => {
      console.log(r);
      this.setState({riderFijo: r })})   */}
      
    {/* inpust rider sala */}
    <div className="forms forms label">
      {/* guardar los datos // guardar los rider de internet en rider sala*/}
      <button className="btn btn-primary" onClick = {e => {
        this.setState({
          sala: {...this.state.sala, rider: this.state.riderFijo}
        })
      }}>Rider Comunes</button>
      {/* pintar los rider que viene de sticht */} 
      {this.state.sala.rider.map( (r, i)=> {
        return(
          <div key={i} > 
           <b>{r.concepto}</b>  
           <input
              type="checkbox"
              //  checked={r.estado}
           onChange={ e => {
              this.setState({
                
              })
           }}/>

           {/* inpust rider sala */}
    <div className="forms forms label">
      {/* guardar los datos // guardar los rider de internet en rider sala*/}
      <button className="btn btn-primary" onClick = {e => {
        this.setState({
          sala: {...this.state.sala, rider: this.state.riderFijo}
        })
      }}>Rider Comunes</button>
      {/* pintar los rider que viene de sticht */} 
      {this.state.sala.rider.map( (r, i)=> {
        return(
          <div key={i} > 
           <b>{r.concepto}</b>  
           <input
              type="checkbox"
              //  checked={r.estado}
           onChange={ e => {
              this.setState({
                
              })
           }}/>
{/* gurdamos la info del value en el array dentro del natributo del objeto * indexgit  */}
           <input 
          className="form-control"
          size="20"
           value={r.comentarios}
           onChange={(e) =>{
             let copia = this.state.sala.rider
             copia[i].comentarios = e.target.value
            //  console.log(copia)
             this.setState({
              sala:{...this.state.sala, rider: copia}
             })
           }}/> 
          </div>
        )
      })}
    

         
  
  
        {JSON.stringify(this.state)}
     </div>
   )
  }
 
  // componentDidMount(){ esto es para que se ejecute nada mas inicilaizar la pageXOffset, cargamos un array pero puede ser cualquier cosa}

  // variables de iconos
    // let iconoGrabaCheck = <i className="material-icons" title="grabar">check_circle</i>
      /* {iconoGrabaCheck} */
      /* <span onClick={ e => {
      document.getElementById('modal').style.display='none'
    }}>cerrar modal</span> */
       {/* <span onClick={ e => {
      document.getElementById('modal').style.display='block'
    }}>abrir modal</span>     */}
    // datos de termial para hacer un commit por archivo git commit -m "incidencia push" src/components/Sala.js
  
}

</div>

como utilizar los hooks

function ShowEntrada(props) {
  // estamos creando el state con los hooks el primer parametro es el aributo
  // del state y el segundo una funcion que sabe que ers una funcion y lo que 
  //tiene que hacer gracias a useState
  var [nombre, setNombre] = useState([])
  var [precio, setPrecio] = useState(0)
  var [entrada, setEntrada] = useState(new Entrada())


  return (
    <div>
       nombre = {JSON.stringify(entrada, null, 2)}
      <button onClick={ e => {
        setEntrada(Object.assign(new Entrada(), {...entrada, nombre: 'Luis'}))
      }}> hola </button>
   
    </div>)

}
  

// como encontrar la longitud de un array se hace con split y devuelve un array


let s0 = '12,23,354,678,34,55'
let s1 = '34,55'
let s3 = '45,67,-12,678'
s0.split(',').length
// para sacar el peniltumo número
s0.split(',')[s0.split(',').length -2]

// para sacar el cuadrado 1º pasasmo el string a un array con split y despues hacemos un map
s0.split(',').map((s,i)=>{return s*s})
s0.split(',').map(s => s*s) // tambien podemos hacerlo así
s0.split(',').map( e => Math.pow(e,2))// podemos utilizar la libreria math que tiene todos los metodos matematicos

let a = [
  { id: 1, nombre: 'Carlos Robles', saldo: 1500 },
  { id: 2, nombre: 'Carmen Torres', saldo: 200 },
  { id: 3, nombre: 'Elvira Huertas', saldo: -300 },
  { id: 4, nombre: 'Luis MartÃ­nez', saldo: 3000 }
]

elemento = find() // esto nos devueve un elemento osea un objeto de un array si no lo encuentra da undefainded
nuemro = findIndex( e => e.saldo <= 0) // esto nos devuelve el indice que ocupa en un arra.
// OJO!! si no lo encuentra el findindex da -1
a.find( e => e.saldo <= 0) // busca en el array el alor del saldo que sea igual o menor que 0

para borrar 
a.splice(a.DatefindIndex( e => e.saldo <= 0), 1) // borra un objeto de un array

let cuadrado = []
a.forEach( e => {
  cuadrado.push( Math.pow(e.saldo, 2))
})

// Para Ordenar el metoso sort  recibe dos parametros de mayor a menor

a.sort((a,b) => a.saldo < b.saldo ? 1 : -1) 

// para filtrar y que muestre solo lo que este dentro de las condiciones

a.filter( e => e.saldo < 0)

// Para BUsqueda por fecha actual que es con new date, lo tengo en esquema bolo.


a. toString() // pasa un array a string
parseFloat('12,3') // pasa string a numeros decimales
parseInt('123') // pasa string a numeros enteros

JSON.stringify({nombre: 'Luis', saldo: 100}) // pasa un json a string 
JSON.parse('{ 'nombre': 'luis'}') // pasa un string a json
