

class Nave {

  constructor( nombre, peso){
    this.nombre = nombre ? nombre : new Date() ;
    this.pesoMaximo = peso ? peso : 0 ; // peso sin Aliens
    this.ocupantes = [];          // los ocupantes siempre son Aliens
    this.posicion = { x: 0, y: 0, z: 0 } // 
  }

  embarca(alien){
    let cabe = this.cuantoPesas() + alien.peso <= this.pesoMaximo
    if ( cabe ) { 
      this.ocupantes.push(alien); 
      return true;
    } else { return false }
  }

  saveToDb(){
    db.naves.insertOne(this);
  }

  cuantoPesas(){
    let total = 0;
    this.ocupantes.forEach( alien => { 
      total += alien.peso
    });
    return total;
  }

  static listado(){
    db.naves.aggregate([]).forEach(printjson);
  }

  static navesPorPesos(orden){
    let menorMayor = orden === -1 ? -1 : 1;
    db.naves.aggregate([
      { $unwind: "$ocupantes" },
      { $group: { 
        _id: "$nombre", 
        peso: { $sum: "$ocupantes.peso" }  ,
        totalOcupantes: { $sum: 1 }
      }},
      { $sort: { peso: menorMayor } }
    ]).forEach(printjson);
  }
}

class Alien {
  constructor(nombre, peso){
    this.nombre = nombre ? nombre : 'Alien desconocido' ;
    this.peso = peso ? peso : 0 ;
  }
}


// db.naves.aggregate([
//   { $unwind: "$ocupantes" },
//   { $group: { 
//     _id: "$nombre", 
//     peso: { $sum: "$ocupantes.peso" }  ,
//     totalOcupantes: { $sum: 1 }
//   }}
// ])

// db.naves.aggregate([
//   { $project: { "ocupantes.nombre": 1, _id: 0 } }
// ])


//Ejemplos de uso:

// n1 = new Nave('Halcón Milenario 25', 3000);
// solo = new Alien('Han Solo', 95);
// solo2 = new Alien('Han Solas', 100);
// chewee = new Alien('Cheewaka', 2800);

// n1.embarca(solo);
// n1.embarca(solo2);
// n1.embarca(chewee);

// n1.saveToDb();

// n1 = new Nave('Halcón Milenario 1', 3000);
// solo = new Alien('Han Solos', 90);
// chewee = new Alien('Cheewakaa', 200);

// n1.embarca(solo);
// n1.embarca(chewee);
// n1.saveToDb();


// n1 = new Nave('Halcón Milenario 2', 3000);
// solo = new Alien('Han Soloss', 90);
// chewee = new Alien('Cheewakaa', 200);
// solo2 = new Alien('Han Solossss', 95);
// solo3 = new Alien('Hansss Solossss', 95);



// n1.embarca(solo);
// n1.embarca(solo2);
// n1.embarca(solo3);

// n1.embarca(chewee);
// n1.saveToDb();