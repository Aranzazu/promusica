import React, { Component } from 'react'
import { mongo, nbsp, br } from '../index'
import { Link } from 'react-router-dom';
import { EsquemaEspectaculo } from './Espectaculo';


export default class ListadoEspectaculos extends Component {

    state = {espectaculos: [], busquedaNombrEspectaculo: ''}

    mongoEspectaculos = mongo.db('proMusic').collection('espectaculos')
    
    getTodosLosEspectaculos = () => {
      this.mongoEspectaculos.aggregate([
        {$sort: {nombre: 1}}
      ]).toArray().then( e => {this.setState({espectaculos:e}); console.log(e)})
    }
// busqueda por nombre
    buscarEspectaculoPorNombre = () => {
        let consulta = [ 
          {$match:{ nombre:{$regex: new RegExp(this.state.busquedaNombrEspectaculo), $options:'i'}}},
          {$sort:{nombre: 1}}
        ]
      this.mongoEspectaculos.aggregate(consulta).toArray().then(d => {this.setState({espectaculos: d})})
    }
    renderButtonAnadiABolo = (espectaculo) =>{

      return(
        <i className="material-icons"
          style={{color:"#0d6e79"}} 
          title="añadir a bolo"
          onClick={ e => { 
            console.log(espectaculo)
            this.props.enviarABolo('espectaculos', espectaculo) 
          }}
           >check_circle</i>
      )   
    }
  

    componentDidMount(){

      this.getTodosLosEspectaculos()
    }

  render() {
    return (
      <div>
        {/* link a espectaculo nuevo y buscador por nombre */}
      <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
      <input className="form-control" placeholder="Nombre espectaculo"
      value={this.state.busquedaNombrEspectaculo}
      onKeyPress = { e => {
        if(e.key === 'Enter'){this.buscarEspectaculoPorNombre()}
      }}
      onChange={ e => {this.setState({
        busquedaNombrEspectaculo: e.target.value
      })}} />
      <i className="material-icons" style={{color:"#0d6e79", fontWeight: "bold"}}
      onClick={ e => {this.buscarEspectaculoPorNombre()}} >
      search
      </i>{nbsp(3)}

        <Link to = {'/espectaculo/nuevo'}  style={{ textDecoration: 'none'}}> <span className="decoracion" >Nuevo</span></Link>
        </div>

        {/* listado espectaculos */}
        <div>
          {this.state.espectaculos.map(
            (e,i) => {
              let espectaculo = Object.assign(new EsquemaEspectaculo(), e)
              return(
              <div key={i}>
                
                <div  className="ficha">
                 
                  <div className="text-center">    
                    <img src={e.imagen} style={{float: "left"}} alt="img" className="fotolistaEspectaculo"/>  
                    <video width="100" height="100" controls style={{float: "right"}} className=" fotolistaEspectaculo">
                      <source src={e.video} type="video/mp4"/>
                    </video>
                    <Link to={`/espectaculo/${e._id}`}>
                    <div  className="text-center h3" >{e.nombre}</div></Link>
                    <a href={e.riderPdf} target="_blank" className="titulo" rel="noopener noreferrer">Rider PDF</a>
                    <div className="titulo">Costes:{nbsp(1)} <span style={{color:"red"}} >{espectaculo.totalCostes()}</span> </div>
                    <div className="text-center ">
                      <div className="h4">Comentarios</div>
                    <span style={{color:"black"}}>{e.comentarios}</span> 
                    </div> 

                    {this.props.desdeBolos ? this.renderButtonAnadiABolo(espectaculo) : <span />}

                  </div>
                  
                 
                </div>
             

              </div>
            )}
          )}
        </div>
        {br(1)}
         <pre>{JSON.stringify(this.state.espectaculos)}</pre> 
      </div>
    )
  }
}

