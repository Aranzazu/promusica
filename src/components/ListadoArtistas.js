import React, { Component } from 'react'
import { mongo, nbsp, br } from '../index'
import { Link } from 'react-router-dom';
import { EsquemaArtistas } from './Artista';


export default class ListadoArtistas extends Component {
  mongoArtistas = mongo.db('proMusic').collection('artistas')

  state = {artistas:[], busquedaNombreArtista:'', competencias:[], competencia: ''}

  componentDidMount(){

    EsquemaArtistas.listadoArtistas().then( a =>{
      this.setState({artistas: a})})
    EsquemaArtistas.listadoCompetencias()
    .then( c => {console.log(c) 
      this.setState({competencias:c})})  
      
    }
  
  changeSelectCompetencias = (e) => {

    this.setState({ competencia: e.target.value }, () =>{
      if ( this.state.competencia === "todas"){
        EsquemaArtistas.listadoArtistas().then(a => {this.setState({artistas:a})})
  }
  else {
        EsquemaArtistas.listadoCompetenciasArtistas(this.state.competencia)
        .then(a => { 
          console.log(a); 
          this.setState({artistas: a})
        })
  }
    })
    console.log(e.target.value)
   
  }
    
  renderButtonAnadirAGrupo = (art) =>{
    return(
      <i className="material-icons"
        style={{color:"#0d6e79"}} 
        title="añadir a grupo"
        onClick={ e => {
          this.props.enviarAGrupo(art)
        }}>check_circle</i>
    )   
  }

  renderSelectCompetencia = () => {
    return(
      <select className="form-control" onChange={this.changeSelectCompetencias}>
        <option value="todas">Competencias</option>
        {this.state.competencias.map(
          (e,i) => 
          <option key={i} value={e._id}> {e._id} </option>
        )}
      </select>
    )
  }

  render() {
    return (
      <div style={{ margin: "10px 15px 10px 15px"}}>
                {/* link a grupo nuevo y buscador por nombre */}
              <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
                {this.renderSelectCompetencia()}{nbsp(2)}
              <input className="form-control" placeholder="Nombre Artista"
              value={this.state.busquedaNombreArtista}
              onKeyPress = { e => {
                if(e.key === 'Enter'){
                  EsquemaArtistas.buscarArtistaPorNombre(this.state.busquedaNombreArtista).then( n => {
                  this.setState({artistas: n})
                })}
              }}
              onChange={ e => {this.setState({
                busquedaNombreArtista: e.target.value
              })}} />
              <i className="material-icons" style={{color:"#0d6e79", fontWeight: "bold"}}
              onClick={ e => {EsquemaArtistas.buscarArtistaPorNombre(this.state.busquedaNombreArtista).then( n => {
                this.setState({ artistas: n})
              })}} >
              search
              </i>{nbsp(3)}
              <Link to = {'/artista/nuevo'}  style={{ textDecoration: 'none'}}> <span className="decoracion" >Nuevo</span></Link>
              </div>

              <div>
              {this.state.artistas.map(
                (a,i) => {
                  let artista = Object.assign(new EsquemaArtistas(), a)
                  return(
                    <div key={i} style={{ margin: "10px 15px 10px 15px"}}>
                      <div className="fichaArtGrup">
                      <Link to={`/artista/${artista._id}`}>
                     <div  className="text-center h3" title={artista.apellido} > {artista.nombre}:{nbsp(1)}<span className="h5">({artista.telefono})</span></div> </Link>
                      
                      {artista.comentario}
                       {artista.competencias.map(
                         (c,i)=>{
                           return( 
                             <div key={i} className="text-center">
                               <span className="h6">{c.competencia}</span>
                             </div>
                           )
                         }
                       )}
                       {this.props.desdeBolos ? this.renderButtonAnadirAGrupo(artista) : <span />}
                      </div>

                    </div>
                    
                  )
                })}
              </div>
                
            </div>
    )
  }
}
